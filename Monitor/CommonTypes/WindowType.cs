﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor.CommonTypes
{
    public enum WindowType
    {
        Rectangle,
        Sin,
        Hann,
        Hamming,
        Blackman,
        Nuttall
    };
}
