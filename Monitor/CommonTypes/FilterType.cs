﻿namespace Monitor.CommonTypes
{
    public enum FilterType
    {
        Passband,
        Stopband,
        Lowpass
    }
}