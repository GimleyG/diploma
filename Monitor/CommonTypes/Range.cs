﻿namespace Monitor.CommonTypes
{
    public struct Range
    {
        public double Min { get; set; }
        public double Max { get; set; }

        public double Width
        {
            get { return Max - Min; }
        }

        public static Range operator+(Range op1, Range op2)
        {
            return new Range { Min = op1.Min + op2.Min, Max = op1.Max + op2.Max };
        }
    }
}