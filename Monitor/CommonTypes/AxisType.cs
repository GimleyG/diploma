﻿namespace Monitor.CommonTypes
{
    public enum AxisType
    {
        Linear = 0,
        Log = 1,
        LogDb
    }
}