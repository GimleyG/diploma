﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Monitor.View.Common;
using Presentation.Common;
using Monitor.CommonTypes;
using Monitor.Model.SignalProcessing;
using Monitor.AppSettings;

namespace Presentation.Presenters
{
    public class FilterSettingsPresenter : BasePresenter<IFilterSettingsView, IFilterParamsSetter>
    {
        private IDrawController _filterPreview;
        private IFilterParamsSetter _paramSetter;

        public FilterSettingsPresenter(IApplicationController controller, IFilterSettingsView view)
            : base(controller, view)
        {
            _filterPreview = View.FilterPreviewGraph;

            SetupView();
            SetupHandlers();
        }        

        private void SetupView()
        {
            var sets = AvailibleFilters.LastModified;

            if (sets.TypeOfFilter == FilterType.Lowpass)
                View.LowRangeAvailability = false;

            View.SelectedFilterType  = sets.TypeOfFilter;
            View.FrequencyRange      = sets.FrequencyRange;
            View.WindowRate          = sets.WinRate;
            View.FilterOrder = sets.FilterOrder;
            View.SelectedChannel     = sets.CurrentChannel;
        }
        private void SetupHandlers()
        {
            View.ConfirmInvoked += OnConfirmInvoked_View;
            View.CalculateInvoked += OnCalculateInvoked_View;
            View.SelectedAxisTypeChanged += OnAxisTypeChanged;
            View.SelectedFilterTypeOrChannelChanged += OnFilterTypeOrChannelChanged;
        }
                       

        #region Обработчики событий формы       

        private void OnCalculateInvoked_View()
        {
            if (CheckInput())
            {                
                var calculateThread = new Thread(() =>
                                {
                                    View.Cursors = Cursors.AppStarting;
                                    var winRate = View.WindowRate;
                                    var range = View.FrequencyRange;
                                    var filterType = View.SelectedFilterType;
                                    int filterOrder = View.FilterOrder;

                                    var values = new double[filterOrder];
                                    unsafe
                                    {
                                        double* data = FiltersController.GetFreqResponse(filterOrder, range.Min, range.Max, winRate, filterType);
                                        for (int i = 0; i < filterOrder; i++)
                                            values[i] = data[i];
                                        FiltersController.FreeDataArray(data);
                                    }
                                    _filterPreview.PointsCount = filterOrder;
                                    _filterPreview.AddDataSample(values);
                                    _filterPreview.Refresh();

                                    double newRange = (range.Width / 2 > 5) ? range.Width / 2 : 5;
                                    _filterPreview.ActualXRange = new Range
                                    {
                                        Min = range.Min - newRange,
                                        Max = range.Max + newRange
                                    };

                                    View.Cursors = Cursors.Default;
                                });
                calculateThread.Start();
            }
        }

        private void OnConfirmInvoked_View()
        {
            if (CheckInput())
            {
                var winRate = View.WindowRate;
                var range = View.FrequencyRange;
                var filterOrder = View.FilterOrder;
                var filterType = View.SelectedFilterType;
                var channel = View.SelectedChannel;

                _paramSetter.SetNewParameters(filterOrder, range, winRate, filterType, channel);
                AvailibleFilters.SaveChanges(filterOrder, range, winRate, filterType, channel);
                View.Close();
            }
        }

        private void OnAxisTypeChanged(AxisType type)
        {
            _filterPreview.TypeOfAxis = type;
        }

        private void OnFilterTypeOrChannelChanged(FilterType type, Channel chn)
        {
            var sets = AvailibleFilters.LoadChangesFor(type, chn);

            if (type == FilterType.Lowpass)
            {
                View.LowRangeAvailability = false;
                var newFreqRange = new Range { Min = 0, Max = sets.FrequencyRange.Max };
                View.FrequencyRange = newFreqRange;
            }
            else
            {
                View.LowRangeAvailability = true;
                View.FrequencyRange = sets.FrequencyRange;
            }
            
            View.WindowRate = sets.WinRate;
            View.FilterOrder = sets.FilterOrder;
        } 

        private bool CheckInput()
        {
            try
            {
                var freqRange = View.FrequencyRange;
                var winRate = View.WindowRate;
                var filterOrder = View.FilterOrder;

                var wrongRange = (freqRange.Min > freqRange.Max) || (freqRange.Max > 125) || (freqRange.Min > 125);
                var wrongWin = (winRate < 0.00001) || (0.1 < winRate);
                var wrongOrder = (filterOrder < 50) || (4999 < filterOrder) ;

                if (wrongRange || wrongWin || wrongOrder)
                {
                    var msg = "Проверьте вводимые значения: диапазон частот должен быть от 0 до 125, ";
                    msg += "коэффициент окна от 0.00001 до 0.1, а порядок фильтра от 50 до 5000";
                    View.ShowMessage(msg, "Ошибка ввода", System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
            }
            catch (FormatException)
            {
                var msg = "Одно из полей ввода пустое!";
                View.ShowMessage(msg, "Ошибка ввода", System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }
            
            return true;            
        }

        #endregion

        public override void Run(IFilterParamsSetter arg)
        {
            _paramSetter = arg;
            View.Show();
        }
    }
}
