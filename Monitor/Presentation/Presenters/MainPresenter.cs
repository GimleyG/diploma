﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

using Monitor.Model.DeviceControl;
using Monitor.Model.SignalProcessing;
using Monitor.View.Common;
using Presentation.Common;
using Monitor.CommonTypes;

namespace Presentation.Presenters
{
    public class MainPresenter : BasePresenter<IMainView>
    {
        private IDataReceiver _service;
        private IDrawController _signalChn1Graph;
        private IDrawController _signalChn2Graph;
        private IDrawController _spectrumGraph;

        private FilterSettings _filterSets;

        public MainPresenter(IApplicationController controller, IMainView view, IDataReceiver service)
            :base(controller, view)
        {
            _service = service;
            _signalChn1Graph = view.SignalChannel1Graph;
            _signalChn2Graph = view.SignalChannel2Graph;
            _spectrumGraph = view.SpectrumGraph;
            
            _filterSets = new FilterSettings();

            SetupDevs();
            SetupView();
            SetupHandlers();            
        }

        private void SetupDevs()
        { 
            var devs = _service.AvailableDevices;
            while (devs.MoveNext())
            {
                View.AddDeviceDescription(devs.Current);
            }

            if (_service.AvailableDeviceCount > 0)
            {
                var speed = _service.AvailableDeviceSpeed;
                while (speed.MoveNext())
                {
                    View.AddDeviceSpeed(speed.Current);
                }
            }
        }

        private void SetupView()
        {           
            View.SelectedSpectrumWinType = WindowType.Rectangle;
            View.SelectedSpectrumChannel = Channel.Chn1;
            View.SpectrumSource = SignalSource.Input;
            View.SignalChannel1Source = SignalSource.Input;
            View.SignalChannel2Source = SignalSource.Input;
            View.MiddleFilterType = FilterType.Passband;
        }

        private void SetupHandlers()
        {
            View.DeviceConnectionPerformed += OnDeviceConnectionPerformed;
            View.DeviceListChanged += OnDeviceListChanged;

            View.SignalTimerTick    += OnSignalTimerTick;
            View.SpectrumTimerTick += OnSpectrumTimerTick;
            View.GraphZooming += OnGraphZooming;
            View.SpectrumNavigationBarScroll += OnSpectrumNavigationBarScroll;
            
            View.FilterActivityChanged += OnFilterActivityChanged;
            View.FilterSettingsInvoked += OnFilterSettingsInvoked;
            View.MiddleFilterTypeChanged += OnMiddleFilterTypeChanged;
            View.SpectrumAxisTypeChanged += OnAxisTypeChanged;

            View.Closing += OnClosingView;

            _service.DataReceived += OnDataReceived;
            _service.DeviceErrorDetected += OnDeviceError;
            _service.DeviceConnected += OnDeviceConnected;
            _service.DeviceDisconnected += OnDeviceDisconnected;

            _filterSets.SettingsChanged += OnNewFilterSets;            
        } 

        #region События модели

        private void OnDataReceived(double[] data)
        {
            double sampleChn1;
            double sampleChn2;
            var middleFilter = View.MiddleFilterType;
            SpectrumCreator.AddSignalSample(data[0], Channel.Chn1, SignalSource.Input);
            SpectrumCreator.AddSignalSample(data[1], Channel.Chn2, SignalSource.Input);


            FiltersController.PutSignalSample(data[0], FilterType.Stopband, Channel.Chn1);
            sampleChn1 = FiltersController.GetFilteredSignalSample(FilterType.Stopband, Channel.Chn1);
            FiltersController.PutSignalSample(sampleChn1, FilterType.Passband, Channel.Chn1);
            FiltersController.PutSignalSample(sampleChn1, FilterType.Lowpass, Channel.Chn1);

            sampleChn1 = FiltersController.GetFilteredSignalSample(middleFilter, Channel.Chn1);



            FiltersController.PutSignalSample(data[1], FilterType.Stopband, Channel.Chn2);
            sampleChn2 = FiltersController.GetFilteredSignalSample(FilterType.Stopband, Channel.Chn2);
            FiltersController.PutSignalSample(sampleChn2, FilterType.Passband, Channel.Chn2);
            FiltersController.PutSignalSample(sampleChn2, FilterType.Lowpass, Channel.Chn2);

            sampleChn2 = FiltersController.GetFilteredSignalSample(middleFilter, Channel.Chn2);



            SpectrumCreator.AddSignalSample(sampleChn1, Channel.Chn1, SignalSource.Output);
            SpectrumCreator.AddSignalSample(sampleChn2, Channel.Chn2, SignalSource.Output);  


            if (FiltersController.IsReady(FilterType.Passband, Channel.Chn1))
            {
                View.FilterStatusLabel = "Фильтр готов";
            }            

            if (View.SignalChannel1Source == SignalSource.Input)
            {
                sampleChn1 = data[0];
            }
            _signalChn1Graph.AddDataSample(new[] { sampleChn1 });
            

            if (View.SignalChannel2Source == SignalSource.Input)
            {
                sampleChn2 = data[1];
            }

            _signalChn2Graph.AddDataSample(new[] { sampleChn2 });                               
        }
        
        private void OnDeviceError(string msg)
        {
            View.ConnectionBtnCapture = "Подключить";
            View.SignalTimerSwitchOn = false;
            View.SpectrumTimerSwitchOn = false;
            View.ShowMessage(msg, "Ошибка подключения", MessageBoxIcon.Error);
        }

        private void OnNewFilterSets()
        {
            var order = _filterSets.FilterOrder;
            var freqRange = _filterSets.FrequencyRange;
            var win = _filterSets.WinRate;
            var type = _filterSets.TypeOfFilter;
            var chn = _filterSets.CurrentChannel;

            FiltersController.SetNewParameters(order, freqRange.Min, freqRange.Max, win, type, chn);           
        }

        private void OnDeviceConnected(string devName)
        {
            View.AddDeviceDescription(devName);
            var speed = _service.AvailableDeviceSpeed;
            while (speed.MoveNext())
            {
                View.AddDeviceSpeed(speed.Current);
            }
            View.StatusLabel = "Доступно " + _service.AvailableDeviceCount + " устройств";
        }

        private void OnDeviceDisconnected(string devName)
        {
            if (_service.ConnectedDeviceDescription == devName && _service.IsConnected)
            {
                DisconnectDevice();
            } 
            View.ClearDeviceInfo(devName);
            View.StatusLabel = "Отключено " + devName ;           
        }

        #endregion

        #region События представления

        private void ConnectDevice()
        {
            _service.ConnectedDeviceDescription = View.SelectedDevice;
            _service.ConnectedDeviceSpeed = View.SelectedDeviceSpeed; 
            _service.StartReading();
            View.ConnectionBtnCapture = "Отключить";
            View.SignalTimerSwitchOn = true;
            View.SpectrumTimerSwitchOn = true;  
        }

        private void DisconnectDevice()
        {
            _service.StopReading();
            View.ConnectionBtnCapture = "Подключить";
            View.SignalTimerSwitchOn = false;
            View.SpectrumTimerSwitchOn = false;  
        }

        private void OnDeviceConnectionPerformed()
        {         
            string infoMsg;
            string filterMsg;
            
            if (_service.IsConnected)
            {
                DisconnectDevice();
                infoMsg = "Отключено " + _service.ConnectedDeviceDescription;
                filterMsg = "Фильтр отключен";               
            }
            else
            {      
                ConnectDevice();
                
                infoMsg = "Подключено " + _service.ConnectedDeviceDescription;
                infoMsg += "; Скорость " + _service.ConnectedDeviceSpeed;
                filterMsg = "Фильтр не активен";                                 
            }

            View.StatusLabel = infoMsg;
            View.FilterStatusLabel = filterMsg;
        }

        private void OnSignalTimerTick()
        {
            _signalChn1Graph.Refresh();
            _signalChn2Graph.Refresh();
        }

        private void OnSpectrumTimerTick()
        {
            var selectedChannel = View.SelectedSpectrumChannel;
            var selectedSource = View.SpectrumSource;
            if (SpectrumCreator.IsReady(selectedChannel, selectedSource))
            {
                Thread findSpectrumThread = new Thread(() =>
                                {
                                    var values = new double[SpectrumCreator.DotCount];
                                    var winType = View.SelectedSpectrumWinType;
                                    unsafe
                                    {
                                        double* data = SpectrumCreator.FindSignalSpectrum(winType, selectedChannel, selectedSource);
                                        for (int i = 0; i < SpectrumCreator.DotCount; i++)
                                            values[i] = data[i];
                                        SpectrumCreator.FreeDataArray(data);
                                    }
                                    _spectrumGraph.AddDataSample(values);
                                    _spectrumGraph.Refresh();
                                });
                findSpectrumThread.Start();
            }
        }

        private void OnDeviceListChanged()
        {
            var updateDev = new Thread(() =>
                        {
                            View.Cursors = Cursors.AppStarting;
                            View.StatusLabel = "Обновление данных об устройствах";

                            Thread.Sleep(4000);
                            _service.Refresh();                            
                            View.Cursors = Cursors.Default;
                        });
            updateDev.Name = "update devices";
            updateDev.Start();
        }

        private void OnFilterActivityChanged(FilterType type, Channel channel)
        {           
            FiltersController.ChangeActiveState(type, channel);                  
            View.ChangeLedFor(FiltersController.IsEnable(type, channel), type, channel);
            View.FilterStatusLabel = (View.FilterStatusLabel == "Фильтр активен") ? "Фильтр не активен" : "Фильтр активен";
        }

        private void OnMiddleFilterTypeChanged()
        {
            var newType = View.MiddleFilterType;
            var lastType = (newType == FilterType.Passband) ? FilterType.Lowpass : FilterType.Passband;

            if (FiltersController.IsEnable(lastType, Channel.Chn1))
            { 
                FiltersController.ChangeActiveState(newType, Channel.Chn1);
                FiltersController.ChangeActiveState(lastType, Channel.Chn1);               
            }

            if (FiltersController.IsEnable(lastType, Channel.Chn2))
            {
                FiltersController.ChangeActiveState(newType, Channel.Chn2);
                FiltersController.ChangeActiveState(lastType, Channel.Chn2);
            }

            if (newType == FilterType.Passband)
            {
                View.ChangeLedTipFor("Активность ПФ", newType, Channel.Chn1);
                View.ChangeLedTipFor("Активность ПФ", newType, Channel.Chn2);
            }
            else
            {
                View.ChangeLedTipFor("Активность ФНЧ", newType, Channel.Chn1);
                View.ChangeLedTipFor("Активность ФНЧ", newType, Channel.Chn2);
            }
        }  

        private void OnFilterSettingsInvoked()
        {
            Controller.Run<FilterSettingsPresenter, IFilterParamsSetter>(_filterSets);
        }
        
        private void OnClosingView()
        {
            DisconnectDevice();

            View.Cursors = Cursors.WaitCursor;
            Thread.Sleep(500);              //чтобы успели завершиться рабочие потоки
            View.Cursors = Cursors.Default;

            SpectrumCreator.DestroySpectrumCreator();
            FiltersController.DestroyFilter(FilterType.Passband, Channel.Chn1);
            FiltersController.DestroyFilter(FilterType.Stopband, Channel.Chn1);
            FiltersController.DestroyFilter(FilterType.Lowpass, Channel.Chn1);

            FiltersController.DestroyFilter(FilterType.Passband, Channel.Chn2);
            FiltersController.DestroyFilter(FilterType.Stopband, Channel.Chn2);
            FiltersController.DestroyFilter(FilterType.Lowpass, Channel.Chn2);
        }

        private void OnGraphZooming(GraphType graph, ZoomType zoom)
        {
            Range range = new Range();
            Func<ZoomType, Range, Range> zoomRange = (ZoomType type, Range r) =>
                {
                    var delta = 0.2 * (r.Max - r.Min);
                    switch (type)
                    {
                        case ZoomType.In:
                            r.Min += delta;
                            r.Max -= delta;
                            break;
                        case ZoomType.Out:
                            r.Min -= delta;
                            r.Max += delta;
                            break;
                    }
                    return r;
                };

            switch (graph)
            {
                case GraphType.SignalChannel1:
                    range = zoomRange(zoom, _signalChn1Graph.ActualXRange);
                    if(range.Width > 1)
                        _signalChn1Graph.ActualXRange = range;
                    break;

                case GraphType.SignalChannel2:
                    range = zoomRange(zoom, _signalChn2Graph.ActualXRange);
                    if (range.Width > 1)
                        _signalChn2Graph.ActualXRange = range;
                    break;

                case GraphType.Spectrum:
                    range = zoomRange(zoom, _spectrumGraph.ActualXRange);
                    if (range.Width > 5)
                    {
                        _spectrumGraph.ActualXRange = range;
                        range = _spectrumGraph.ActualXRange;
                        View.SpectrumNavigationBarRange = range;
                        View.SpectrumNavigationBarVisible = (range.Width != 125);
                    }
                    break;
            }        
        }

        private void OnSpectrumNavigationBarScroll()
        {
            _spectrumGraph.ActualXRange = View.SpectrumNavigationBarRange;
        }
        
        private void OnAxisTypeChanged(AxisType type)
        {
            _spectrumGraph.TypeOfAxis = type;
            _spectrumGraph.TitleOfYAxis = (type == AxisType.LogDb) ? "U, dB" : "U, mV";            
        } 

        #endregion
        

        public override void Run()
        {
            SpectrumCreator.InitSpectrumCreator(4096);
            FiltersController.InitFilter(400, 7.5, 12.5, 0.04, FilterType.Passband, Channel.Chn1);   
            FiltersController.InitFilter(2048, 49, 51, 0.001, FilterType.Stopband, Channel.Chn1);
            FiltersController.InitFilter(500, 0, 20, 0.04, FilterType.Lowpass, Channel.Chn1);

            FiltersController.InitFilter(400, 7.5, 12.5, 0.04, FilterType.Passband, Channel.Chn2);
            FiltersController.InitFilter(2048, 49, 51, 0.001, FilterType.Stopband, Channel.Chn2);
            FiltersController.InitFilter(500, 0, 20, 0.04, FilterType.Lowpass, Channel.Chn2);
            View.Show();
        }
    }
}