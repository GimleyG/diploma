﻿using System;
using System.Windows.Forms;

using Presentation.Common;
using Presentation.Presenters;
using Monitor.View.Common;
using Monitor.Model.DeviceControl;
using Monitor.View;

namespace Monitor
{
    static class Program
    {

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var controller = new ApplicationController(new LightInjectAdapter())
                .RegisterView<IMainView, MainForm>()
                .RegisterView<IFilterSettingsView, FilterSettingsForm>()
                .RegisterService<IDataReceiver, DataReceiver>()
                .RegisterInstance(new ApplicationContext());

            controller.Run<MainPresenter>();
        }
    }
}
