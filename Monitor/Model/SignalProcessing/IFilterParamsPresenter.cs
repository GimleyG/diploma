﻿using Monitor.CommonTypes;

namespace Monitor.Model.SignalProcessing
{
    public interface IFilterParamsPresenter
    {
        FilterType TypeOfFilter { get; }
        Channel CurrentChannel { get; }
        int FilterOrder { get; }
        Range FrequencyRange { get; }
        double WinRate { get; }
    }
}
