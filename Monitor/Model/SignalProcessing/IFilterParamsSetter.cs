﻿using Monitor.CommonTypes;

namespace Monitor.Model.SignalProcessing
{
    public interface IFilterParamsSetter
    {
        void SetNewParameters(int order, Range freqRange, double winRate, FilterType type, Channel channel);
    }
}
