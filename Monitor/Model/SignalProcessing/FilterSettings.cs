﻿using System;
using Monitor.CommonTypes;

namespace Monitor.Model.SignalProcessing
{
    public class FilterSettings : IFilterParamsSetter, IFilterParamsPresenter 
    {
        public FilterType TypeOfFilter { get; set; }
        public Channel CurrentChannel { get; set; }
        public int FilterOrder { get; set; }
        public Range FrequencyRange { get; set; }
        public double WinRate { get; set; }

        public FilterSettings()
        { 
            TypeOfFilter = FilterType.Passband;
            CurrentChannel = Channel.Chn1;
            FilterOrder = 2048;
            FrequencyRange = new Range{Min = 8, Max = 10};
            WinRate = 0.1;
        }

        public event Action SettingsChanged;
        private void OnSettingsChanged()
        {
            if (SettingsChanged != null)
            {
                SettingsChanged();
            }
        }

        public void SetNewParameters(int order, Range freqRange, double winRate, FilterType type, Channel channel)
        {
            FilterOrder = order;
            FrequencyRange = freqRange;
            WinRate = winRate;
            TypeOfFilter = type;
            CurrentChannel = channel;

            OnSettingsChanged();
        }
    }
}
