﻿using Monitor.CommonTypes;
using System;
using System.Runtime.InteropServices;

namespace Monitor.Model.SignalProcessing
{
    public class SpectrumCreator
    {
        public const int DotCount = 2048;
        private const string dllname = "SpectrumCreator.dll";

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void InitSpectrumCreator(int capacity);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void DestroySpectrumCreator();

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void AddSignalSample(double sample, Channel channelNumber, SignalSource source);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern bool IsReady(Channel channelNumber, SignalSource source);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe double* FindSignalSpectrum(WindowType type, Channel channelNumber, SignalSource source);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe void FreeDataArray(double* array);
    }
}