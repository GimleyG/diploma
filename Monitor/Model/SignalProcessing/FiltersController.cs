﻿using Monitor.CommonTypes;
using System.Runtime.InteropServices;

namespace Monitor.Model.SignalProcessing
{
    public class FiltersController
    {
        private const string dllname = "FiltersController.dll";

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void InitFilter(int coeffsCount, double downBandFreq, double upBandFreq, double winFactor, FilterType type, Channel chn);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void SetNewParameters(int coeffsCount, double downBandFreq, double upBandFreq, double winFactor, FilterType type, Channel chn);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void DestroyFilter(FilterType type, Channel chn);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void PutSignalSample(double sample, FilterType type, Channel chn);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern double GetFilteredSignalSample(FilterType type, Channel chn);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void ChangeActiveState(FilterType type, Channel chn);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern bool IsReady(FilterType type, Channel chn);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern bool IsEnable(FilterType type, Channel chn);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe double* GetFreqResponse(int coeffsCount, double downBandFreq, double upBandFreq, double winFactor, FilterType type);

        [DllImport(dllname, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe void FreeDataArray(double* array);
    }
}