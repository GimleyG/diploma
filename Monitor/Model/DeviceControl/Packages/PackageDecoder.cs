﻿using System;
using System.Runtime.Serialization.Formatters.Binary;

namespace Monitor.Model.DeviceControl.Packages
{
    
    /// <summary>
    /// Разбор пакета на данные по каналам
    /// </summary>
    public class PackageDecoder
    {
        /// <summary>
        /// Количество каналов
        /// </summary>
        private int _channelCount;
        
        /// <summary>
        /// Создание объекта с настройкой количества каналов.
        /// Количество каналов берется из расчета, что 
        /// на каждый канал из пакета приходится 4 байта данных
        /// </summary>
        /// <param name="channelCount">Количество каналов</param>
        public PackageDecoder(int channelCount)
        {
            _channelCount = channelCount;
        }
        

        public event Action<double[]> ChannelValuesDecode;
        private void OnChannelValuesDecode(double[] data)
        {
            if (ChannelValuesDecode != null)
            {
                ChannelValuesDecode(data);
            }
        }

        
        public void DecodePackage(Package package)
        {
            var numeric = new byte[4];
            var values = new double[_channelCount];

            for (var chanNum = 0; chanNum < _channelCount; chanNum++)
            {
                //записываем в обратном порядке, т.к при конвертации используется little-endian
                for (int i = 3, offset = 1; i >= 0; i--, offset++)
                {
                    //offset необходим т.к в пакете первый байт данных - номер пакета в потоке
                    var bytePos = chanNum*4 + offset;   
                    numeric[i] = package.Data[bytePos];
                }

                values[chanNum] = BitConverter.ToInt32(numeric, 0);
            }
            OnChannelValuesDecode(values);
        }
    }
}