﻿using System;

namespace Monitor.Model.DeviceControl.Packages
{
    public struct Package
    {
        public byte Address;
        public byte Cmd;
        public byte DataLength;
        public byte[] Data;

        public override string ToString()
        {
            string result = Address + " " + Cmd + " " + DataLength + " ";
            result += BitConverter.ToString(Data);
            
            return result;
        }
    }
}