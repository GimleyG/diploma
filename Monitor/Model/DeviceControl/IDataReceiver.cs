﻿using System;
using System.Collections.Generic;
using Monitor.Model.DeviceControl.Device;
using Monitor.Model.DeviceControl.Packages;

namespace Monitor.Model.DeviceControl
{
    public interface IDataReceiver
    {
        event Action<Package>  PackageReceived;
        event Action<double[]> DataReceived;
        event Action<string> DeviceErrorDetected;
        event Action<string> DeviceConnected;
        event Action<string> DeviceDisconnected;

        string ConnectedDeviceDescription { get; set; }
        uint ConnectedDeviceSpeed { get; set; }
        bool IsConnected { get; }
        IEnumerator<string> AvailableDevices { get; }
        IEnumerator<uint> AvailableDeviceSpeed { get; }
        uint AvailableDeviceCount { get; }

        void StartReading();
        void StopReading();
        void Refresh();
    }
}