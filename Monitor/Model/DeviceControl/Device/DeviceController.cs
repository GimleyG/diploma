﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

using Monitor.Model.DeviceControl.Packages;

using FT_HANDLE = System.UInt32;

namespace Monitor.Model.DeviceControl.Device
{
    /// <summary>
    /// Настройка и управление устройством
    /// </summary>
    public class DeviceController
    {
        private const uint SPEED_57600 = 57600;
        private const uint SPEED_230400 = 230400;
        private const uint SPEED_288000 = 288000;


        #region Поля
        /// <summary>
        /// Список доступных устройств
        /// </summary>
        private List<DeviceInfo> _devices;

        /// <summary>
        /// Скорость чтения
        /// </summary>
        private uint _deviceSpeed;

        /// <summary>
        /// Количество доступных устройств
        /// </summary>
        private uint _deviceCount;

        /// <summary>
        /// Подключенное устройство
        /// </summary>
        private DeviceInfo _connectedDevice;

        /// <summary>
        /// Поток для чтения с устройства
        /// </summary>
        private Thread _readingThread;

        /// <summary>
        /// Флаг продолжения чтения
        /// </summary>
        private bool _isConnected;
        
        #endregion



        /// <summary>
        /// Событие получения пакета
        /// </summary>
        public event Action<Package> PackageReceive;

        public event Action<string> DeviceErrorDetected;

        public event Action<string> DeviceConnected;

        public event Action<string> DeviceDisconnected;

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public DeviceController()
        {
            _deviceCount = 0;
            _devices = new List<DeviceInfo>();
            _deviceSpeed = SPEED_57600;
            _connectedDevice = new DeviceInfo();

            _isConnected = false;

            UpdateDeviceInfoList();
        }


        /// <summary>
        /// Обновление данных о подключенных устройствах
        /// </summary>
        public void UpdateDeviceInfoList()
        {
            var newDevs = new List<DeviceInfo>();
            
            FT_CreateDeviceInfoList(ref _deviceCount);

            byte[] sernum = new byte[16];
            byte[] desc = new byte[64];

            for (UInt32 i = 0; i < _deviceCount; i++)
            {
                var device = new DeviceInfo();
                FT_GetDeviceInfoDetail(i,
                                        ref device.Flags,
                                        ref device.Type,
                                        ref device.Id,
                                        ref device.LocId,
                                        sernum,
                                        desc,
                                        IntPtr.Zero
                                      );

                var serial = Encoding.ASCII.GetString(sernum);
                serial = serial.Substring(0, serial.IndexOf("\0", StringComparison.Ordinal));
                device.SerialNumber = String.IsNullOrEmpty(serial) ? "0" : serial;

                var description = Encoding.ASCII.GetString(desc);
                description = description.Substring(0, description.IndexOf("\0", StringComparison.Ordinal));
                device.Description = String.IsNullOrEmpty(description) ? "<no name>" : description;

                device.Index = i;
                newDevs.Add(device);
            }
            CheckChanges(newDevs);
            _devices = newDevs;
        }

        private void CheckChanges(List<DeviceInfo> newDevs)
        {
            if (_devices.Count == newDevs.Count) return;

            if (_devices.Count > newDevs.Count)
            {
                var enumerator = _devices.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (newDevs.Contains(enumerator.Current))
                        continue;
                    OnDeviceDisconnected(enumerator.Current.Description);
                }                
            }
            else
            {
                var enumerator = newDevs.GetEnumerator();
                while (enumerator.MoveNext()) 
                {
                    if (_devices.Contains(enumerator.Current))
                        continue;
                    OnDeviceConnected(enumerator.Current.Description);
                }                           
            }        
        }

        
        #region Свойства

        /// <summary>
        /// Количество подключенных устройств
        /// </summary>
        public uint DeviceCount
        {
            get
            {
                return _deviceCount;
            }
        }

        public bool IsDeviceConnected
        {
            get { return _isConnected; }
        }

        /// <summary>
        /// Информация о подключенных устройствах
        /// </summary>
        public IEnumerator<string> AvailableDevices
        {
            get
            {
                return _devices
                       .Select(n => n.Description)
                       .GetEnumerator();
            }
        }

        public IEnumerator<uint> AvailableDeviceSpeed
        {
            get
            {
                var speed = new List<uint>();
                speed.Add(SPEED_57600);
                speed.Add(SPEED_230400);
                speed.Add(SPEED_288000);
                return speed.GetEnumerator();
            }
        }

        /// <summary>
        /// Описание текущего подключенного устройства
        /// </summary>
        public string ConnectedDeviceDescription
        {
            get { return _connectedDevice.Description; }
            set
            {
                var enumerator = _devices.GetEnumerator();
                do
                {
                    if (value.Equals(enumerator.Current.Description))
                    {
                        _connectedDevice = enumerator.Current;
                        break;
                    }
                } while (enumerator.MoveNext());
            }
        }

        public uint DeviceSpeed
        {
            get { return _deviceSpeed;}
            set { _deviceSpeed = value; }
        }
        


        #endregion
        
        #region Методы

        
        /// <summary>
        /// Чтение пакетов с устройства
        /// </summary>
        private void ReadingFromDevice()
        {
            int index = (int)_connectedDevice.Index;
            try
            {
                if (!AccessUSB(index))
                {
                    _isConnected = false;
                    throw new Exception("Невозможно подключиться к выбранному устройству");
                }
                OpenUSB(index, _deviceSpeed);
                PurgeUSB();

                ReadingCycle();
            }
            catch (Exception ex)
            {
                OnDeviceError(ex.Message);
            }
            finally
            {
                PurgeUSB();
                CloseUSB();
            }

        }

        /// <summary>
        /// Основной цикл чтения
        /// </summary>
        private void ReadingCycle()
        {
            var package = new Package { Data = new byte[1] };
            var oldSize = 1;
            while (_isConnected)
            {
                RxFrameUSB(_connectedDevice.Index, ref package.Address, ref package.Cmd, ref package.DataLength, package.Data);
                var newSize = package.DataLength;

                if (newSize == 0)
                {
                    _isConnected = false;
                    throw new Exception("Нет данных! Возможно неверно задана скорость подключения");
                }

                if (newSize != oldSize)
                {
                    package.Data = new byte[package.DataLength];
                    oldSize = package.DataLength;
                    continue;
                }
                OnPackageReceive(package);
            }
        }
        
        /// <summary>
        /// Запуск чтения с порта
        /// </summary>
        public void StartReading()
        {
            _isConnected = true;
            CreateReadingThread();
            _readingThread.Start();
        }

        /// <summary>
        /// Создание и настройка читающего потока
        /// </summary>
        private void CreateReadingThread()
        {
            _readingThread = new Thread(ReadingFromDevice)
                {
                    Name = "Reading thread",
                    IsBackground = true,
                    Priority = ThreadPriority.Highest
                };
        }

        /// <summary>
        /// Остановка чтения с порта
        /// </summary>
        public void StopReading()
        {
            _isConnected = false;
            _readingThread = null;
        }
        

        /// <summary>
        /// Создание события получения пакета 
        /// </summary>
        /// <param name="package">Входящий пакет</param>
        private void OnPackageReceive(Package package)
        {
            if (PackageReceive != null)
            {
                PackageReceive(package);
            }
        }

        /// <summary>
        /// Создание события ошибки устройства
        /// </summary>
        /// <param name="error">Сообщение об ошибке</param>
        private void OnDeviceError(string error)
        {
            if (DeviceErrorDetected != null)
            {
                DeviceErrorDetected(error);
            }
        }

        private void OnDeviceConnected(string devName)
        {
            if (DeviceConnected != null)
            {
                DeviceConnected(devName);
            }
        }

        private void OnDeviceDisconnected(string devName)
        {
            if (DeviceDisconnected != null)
            {
                DeviceDisconnected(devName);
            }
        }

        #endregion

        #region Dll imported functions

        [DllImport("FTD2XX.dll")]
        static extern uint FT_CreateDeviceInfoList(ref UInt32 numDevs);
        
        [DllImport("FTD2XX.dll")]
        static extern uint FT_GetDeviceInfoDetail(UInt32 dwIndex, ref UInt32 flags, ref UInt32 type, ref UInt32 id, ref UInt32 locId, byte[] pcSerialNumber, byte[] pcDescription, IntPtr ftHandle);

        [DllImport("wusb32.dll")]
        static extern bool AccessUSB(int devNum);

        [DllImport("wusb32.dll")]
        static extern bool OpenUSB(int devNum, UInt32 baud);

        [DllImport("wusb32.dll")]
        static extern bool CloseUSB();

        [DllImport("wusb32.dll")]
        static extern bool PurgeUSB();

        [DllImport("wusb32.dll")]
        static extern bool RxFrameUSB(UInt32 to, ref byte adr, ref byte cmd, ref byte n, byte[] data);

        #endregion
    }
}
