﻿using System;
using System.Runtime.InteropServices;

namespace Monitor.Model.DeviceControl.Device
{
    /// <summary>
    /// Информация об устройстве
    /// </summary>
    internal struct DeviceInfo
    {
        /// <summary>
        /// Номер устройства в системе
        /// </summary>
        public UInt32 Index;

        /// <summary>
        /// Флаг
        /// </summary>
        public UInt32 Flags;

        /// <summary>
        /// Тип устройства
        /// </summary>
        public UInt32 Type;

        /// <summary>
        /// Идентификатор устройства
        /// </summary>
        public UInt32 Id;

        /// <summary>
        /// Локальный идентификатор
        /// </summary>
        public UInt32 LocId;

        /// <summary>
        /// Серийный номер устройства
        /// </summary>
        public string SerialNumber;

        /// <summary>
        /// Описание устройства
        /// </summary>
        public string Description;
    }
}