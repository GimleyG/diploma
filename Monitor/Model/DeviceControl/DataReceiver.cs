﻿using System;
using System.Collections.Generic;
using Monitor.Model.DeviceControl.Device;
using Monitor.Model.DeviceControl.Packages;

namespace Monitor.Model.DeviceControl
{
    public class DataReceiver : IDataReceiver
    {
        private const int SignalRate = 1000000;
        private const int ChannelCount = 2;
        private DeviceController _devController;
        private PackageDecoder _decoder;
        

        public DataReceiver()
        {
            _devController = new DeviceController();
            _decoder = new PackageDecoder(ChannelCount);

            _devController.PackageReceive += _decoder.DecodePackage;
            _devController.PackageReceive += OnPackageReceived;
            _devController.DeviceErrorDetected += OnDeviceErrorDetected;
            _devController.DeviceConnected += OnDeviceConnected;
            _devController.DeviceDisconnected += OnDeviceDisconnected;

            _decoder.ChannelValuesDecode += OnChannelValuesDecode;
        }

        private void OnChannelValuesDecode(double[] values)
        {
            if (DataReceived != null)
            {
                DataReceived(new []{ values[0]/SignalRate, values[1]/SignalRate});
            }
        }

        private void OnPackageReceived(Package package)
        {
            if (PackageReceived != null)
            {
                PackageReceived(package);
            }
        }

        private void OnDeviceErrorDetected(string error)
        {
            if (DeviceErrorDetected != null)
            {
                DeviceErrorDetected(error);
            }
        }
        
        private void OnDeviceConnected(string devName)
        {
            if (DeviceConnected != null)
            {
                DeviceConnected(devName);
            }
        }

        private void OnDeviceDisconnected(string devName)
        {
            if (DeviceDisconnected != null)
            {
                DeviceDisconnected(devName);
            }
        }

        #region Реализация IDataReceiver
        
        public event Action<Package> PackageReceived;
        public event Action<double[]> DataReceived;
        public event Action<string> DeviceErrorDetected;
        public event Action<string> DeviceConnected;
        public event Action<string> DeviceDisconnected;

        public void StartReading()
        {
            _devController.StartReading();
        }

        public void StopReading()
        {
            _devController.StopReading();
        }

        public void Refresh()
        {
            _devController.UpdateDeviceInfoList();
        }


        public string ConnectedDeviceDescription
        {
            get { return _devController.ConnectedDeviceDescription; }
            set { _devController.ConnectedDeviceDescription = value; }
        }

        public uint ConnectedDeviceSpeed
        {
            get { return _devController.DeviceSpeed; }
            set { _devController.DeviceSpeed = value; }
        }
        
        public bool IsConnected
        {
            get { return _devController.IsDeviceConnected; }
        }
        
        public IEnumerator<string> AvailableDevices
        {
            get { return _devController.AvailableDevices; }
        }

        public IEnumerator<uint> AvailableDeviceSpeed
        {
            get { return _devController.AvailableDeviceSpeed; }
        }

        public uint AvailableDeviceCount
        {
            get { return _devController.DeviceCount; }
        }
        
        #endregion



        
    }
}