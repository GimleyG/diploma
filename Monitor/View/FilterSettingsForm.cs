﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Monitor.View.Common;
using Monitor.View.DrawControllers;
using Monitor.CommonTypes;

namespace Monitor.View
{
    public partial class FilterSettingsForm : BaseForm, IFilterSettingsView
    {
        private IDrawController _filterPreview;
        private readonly ApplicationContext _context;

        public FilterSettingsForm(ApplicationContext context)
        {
            _context = context;

            InitializeComponent();
            _filterPreview = new FilterPreviewDrawController(filterPreviewGraph);
            scaleTypeSetter.SelectedIndex = 0;
        }


        #region События формы
        private void OnConfirmInvoked(object sender, EventArgs e)
        {
            RethrowAction(ConfirmInvoked);            
        }

        private void OnCalculateInvoked(object sender, EventArgs e)
        {
            RethrowAction(CalculateInvoked);
        }

        private void OnSelectedAxisTypeChanged(object sender, EventArgs e)
        {
            AxisType axisType = (AxisType)scaleTypeSetter.SelectedIndex;
            RethrowAction<AxisType>(SelectedAxisTypeChanged, axisType);
        }

        private void OnSelectedFilterTypeOrChannelChanged(object sender, EventArgs e)
        {
            var filterType = (FilterType)filterTypeSelector.SelectedIndex;
            var channel = (chn1RadioBtn.Checked) ? Channel.Chn1 : Channel.Chn2;
            RethrowAction<FilterType, Channel>(SelectedFilterTypeOrChannelChanged, filterType, channel);
        }     

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region Реализация интерфейса IFilterSettingsView

        void IView.Show()
        {
            base.Show();
        }
        void IView.Close()
        {
            this.Close();
        }

        public event Action ConfirmInvoked;
        public event Action CalculateInvoked;
        public event Action<AxisType> SelectedAxisTypeChanged;
        public event Action<FilterType, Channel> SelectedFilterTypeOrChannelChanged;

        public FilterType SelectedFilterType
        {
            get
            {
                return TryInvoke<FilterType>(filterTypeSelector, () => { return (FilterType)filterTypeSelector.SelectedIndex; });
            }
            set
            {
                filterTypeSelector.SelectedIndex = (int)value;
            }
        }
        public Range FrequencyRange
        {
            get
            {
                return new Range { Min = Convert.ToDouble(downFreqTextBox.Text),
                                   Max = Convert.ToDouble(upFreqTextBox.Text)};
            }
            set
            {
                downFreqTextBox.Text = Convert.ToString(value.Min);
                upFreqTextBox.Text = Convert.ToString(value.Max);
            }
        }
        public double WindowRate
        {
            get
            {
                return Convert.ToDouble(winRateTextBox.Text);
            }
            set
            {
                winRateTextBox.Text = Convert.ToString(value);
            }
        }
        public IDrawController FilterPreviewGraph
        {
            get { return _filterPreview; }
        }
        public int FilterOrder
        {
            get
            {
                return TryInvoke<int>(filterOrderTextBox, () => { return Convert.ToInt32(filterOrderTextBox.Text); });
            }
            set
            {
                filterOrderTextBox.Text = Convert.ToString(value);
            }
        }
        public Channel SelectedChannel
        {
            get
            {
                return (chn1RadioBtn.Checked) ? Channel.Chn1 : Channel.Chn2;
            }
            set
            {
                chn1RadioBtn.Checked = (value == Channel.Chn1);
                chn2RadioBtn.Checked = (value == Channel.Chn2);
            }
        }
        public bool LowRangeAvailability
        {
            get
            {
                return downFreqTextBox.Enabled;
            }
            set
            {
                downFreqTextBox.Enabled = value;
            }
        }

        public DialogResult ShowMessage(string msg, string caption, MessageBoxIcon icon)
        {
            return MessageBox.Show(msg, caption, MessageBoxButtons.OK, icon);
        }
        public DialogResult ShowMessage(string msg, string caption, MessageBoxIcon icon, MessageBoxButtons btns)
        {
            return MessageBox.Show(msg, caption, btns, icon);
        }
        public Cursor Cursors
        {
            get { return this.Cursor; }
            set
            {
                TryInvoke(this, () => Cursor = value);
            }
        }
                
        #endregion       

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {       
            var tb = sender as TextBox;
            var text = tb.Text;
            var selLength = tb.SelectionLength;
            
            int intPartSize = 0;
            int fracPartSize = 0;

            var pressedDot = (uint)e.KeyChar == 44;
            var pressedBackspace = (Keys)e.KeyChar == Keys.Back;
            var pressedNumber = 47 < (uint)e.KeyChar && (uint)e.KeyChar < 58;

            switch((string)tb.Tag)
            {
                case "Order":
                    intPartSize = 4;
                    fracPartSize = 0;
                    pressedDot = false;
                    break;
                case "DownFreq":
                case "UpFreq":
                    intPartSize = 3;
                    fracPartSize = 2;
                    break;
                case "WinRate":
                    intPartSize = 1;
                    fracPartSize = 5;
                    break;
            }          
            if ( (selLength > 0) && (pressedDot || pressedBackspace || pressedNumber))
                return;

            //Backspace проходит
            if (pressedBackspace)
                return;
            //проверка что точка одна
            if(pressedDot)
            {
                foreach (char ch in text)
                {
                    if ((uint)ch == 44)
                        e.Handled = true;
                }
                return;
            }
            //проверка что это символ и соблюдения маски 000.00 
            var dotPos = text.IndexOf(',');
            var tooBigIntOrFractPart = (dotPos < 0 && text.Length + 1 > intPartSize) || (dotPos >= 0 && (text.Length - dotPos > fracPartSize));
            
            if ( !pressedNumber || tooBigIntOrFractPart)
            {                
                e.Handled = true;
            }
            else
                return;
        }


        
    }
}
