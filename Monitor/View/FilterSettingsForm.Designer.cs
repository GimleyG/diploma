﻿namespace Monitor.View
{
    partial class FilterSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.filterPreviewGraph = new ZedGraph.ZedGraphControl();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.filterTypeSelector = new System.Windows.Forms.ComboBox();
            this.downFreqTextBox = new System.Windows.Forms.TextBox();
            this.upFreqTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.scaleTypeSetter = new System.Windows.Forms.ComboBox();
            this.calculateBtn = new System.Windows.Forms.Button();
            this.winRateTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.filterOrderTextBox = new System.Windows.Forms.TextBox();
            this.channelsSetGroup = new System.Windows.Forms.GroupBox();
            this.chn2RadioBtn = new System.Windows.Forms.RadioButton();
            this.chn1RadioBtn = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.channelsSetGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // filterPreviewGraph
            // 
            this.filterPreviewGraph.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.filterPreviewGraph.Dock = System.Windows.Forms.DockStyle.Top;
            this.filterPreviewGraph.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.filterPreviewGraph.IsEnableHZoom = false;
            this.filterPreviewGraph.IsEnableVZoom = false;
            this.filterPreviewGraph.Location = new System.Drawing.Point(0, 0);
            this.filterPreviewGraph.Name = "filterPreviewGraph";
            this.filterPreviewGraph.ScrollGrace = 0D;
            this.filterPreviewGraph.ScrollMaxX = 0D;
            this.filterPreviewGraph.ScrollMaxY = 0D;
            this.filterPreviewGraph.ScrollMaxY2 = 0D;
            this.filterPreviewGraph.ScrollMinX = 0D;
            this.filterPreviewGraph.ScrollMinY = 0D;
            this.filterPreviewGraph.ScrollMinY2 = 0D;
            this.filterPreviewGraph.Size = new System.Drawing.Size(490, 234);
            this.filterPreviewGraph.TabIndex = 2;
            // 
            // confirmBtn
            // 
            this.confirmBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.confirmBtn.Location = new System.Drawing.Point(415, 247);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(75, 23);
            this.confirmBtn.TabIndex = 3;
            this.confirmBtn.Text = "Принять";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.OnConfirmInvoked);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelBtn.Location = new System.Drawing.Point(334, 247);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 4;
            this.cancelBtn.Text = "Отмена";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Тип фильтра";
            // 
            // filterTypeSelector
            // 
            this.filterTypeSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filterTypeSelector.FormattingEnabled = true;
            this.filterTypeSelector.Items.AddRange(new object[] {
            "Полосовой",
            "Режекторный",
            "Нижних частот"});
            this.filterTypeSelector.Location = new System.Drawing.Point(117, 60);
            this.filterTypeSelector.Name = "filterTypeSelector";
            this.filterTypeSelector.Size = new System.Drawing.Size(105, 21);
            this.filterTypeSelector.TabIndex = 6;
            this.filterTypeSelector.SelectedIndexChanged += new System.EventHandler(this.OnSelectedFilterTypeOrChannelChanged);
            // 
            // downFreqTextBox
            // 
            this.downFreqTextBox.Location = new System.Drawing.Point(162, 129);
            this.downFreqTextBox.MaxLength = 6;
            this.downFreqTextBox.Name = "downFreqTextBox";
            this.downFreqTextBox.Size = new System.Drawing.Size(60, 20);
            this.downFreqTextBox.TabIndex = 7;
            this.downFreqTextBox.Tag = "DownFreq";
            this.downFreqTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // upFreqTextBox
            // 
            this.upFreqTextBox.Location = new System.Drawing.Point(162, 155);
            this.upFreqTextBox.MaxLength = 6;
            this.upFreqTextBox.Name = "upFreqTextBox";
            this.upFreqTextBox.Size = new System.Drawing.Size(60, 20);
            this.upFreqTextBox.TabIndex = 8;
            this.upFreqTextBox.Tag = "UpFreq";
            this.upFreqTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Диапазон частот";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(128, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "fн = ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(128, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "fв = ";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(7, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 29);
            this.label5.TabIndex = 12;
            this.label5.Text = "Коэффициент окна сглаживания";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.scaleTypeSetter);
            this.panel1.Controls.Add(this.cancelBtn);
            this.panel1.Controls.Add(this.confirmBtn);
            this.panel1.Controls.Add(this.filterPreviewGraph);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(228, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(494, 277);
            this.panel1.TabIndex = 14;
            // 
            // scaleTypeSetter
            // 
            this.scaleTypeSetter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scaleTypeSetter.FormattingEnabled = true;
            this.scaleTypeSetter.Items.AddRange(new object[] {
            "Линейная",
            "Логарифмическая"});
            this.scaleTypeSetter.Location = new System.Drawing.Point(3, 247);
            this.scaleTypeSetter.Name = "scaleTypeSetter";
            this.scaleTypeSetter.Size = new System.Drawing.Size(121, 21);
            this.scaleTypeSetter.TabIndex = 5;
            this.scaleTypeSetter.SelectedIndexChanged += new System.EventHandler(this.OnSelectedAxisTypeChanged);
            // 
            // calculateBtn
            // 
            this.calculateBtn.Location = new System.Drawing.Point(147, 242);
            this.calculateBtn.Name = "calculateBtn";
            this.calculateBtn.Size = new System.Drawing.Size(75, 23);
            this.calculateBtn.TabIndex = 15;
            this.calculateBtn.Text = "Рассчитать";
            this.calculateBtn.UseVisualStyleBackColor = true;
            this.calculateBtn.Click += new System.EventHandler(this.OnCalculateInvoked);
            // 
            // winRateTextBox
            // 
            this.winRateTextBox.Location = new System.Drawing.Point(162, 194);
            this.winRateTextBox.MaxLength = 7;
            this.winRateTextBox.Name = "winRateTextBox";
            this.winRateTextBox.Size = new System.Drawing.Size(60, 20);
            this.winRateTextBox.TabIndex = 13;
            this.winRateTextBox.Tag = "WinRate";
            this.winRateTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Порядок фильтра";
            // 
            // filterOrderTextBox
            // 
            this.filterOrderTextBox.Location = new System.Drawing.Point(162, 96);
            this.filterOrderTextBox.MaxLength = 4;
            this.filterOrderTextBox.Name = "filterOrderTextBox";
            this.filterOrderTextBox.Size = new System.Drawing.Size(60, 20);
            this.filterOrderTextBox.TabIndex = 17;
            this.filterOrderTextBox.Tag = "Order";
            this.filterOrderTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // channelsSetGroup
            // 
            this.channelsSetGroup.Controls.Add(this.chn2RadioBtn);
            this.channelsSetGroup.Controls.Add(this.chn1RadioBtn);
            this.channelsSetGroup.Location = new System.Drawing.Point(10, 12);
            this.channelsSetGroup.Name = "channelsSetGroup";
            this.channelsSetGroup.Size = new System.Drawing.Size(212, 36);
            this.channelsSetGroup.TabIndex = 21;
            this.channelsSetGroup.TabStop = false;
            this.channelsSetGroup.Text = "Канал";
            // 
            // chn2RadioBtn
            // 
            this.chn2RadioBtn.AutoSize = true;
            this.chn2RadioBtn.Location = new System.Drawing.Point(121, 13);
            this.chn2RadioBtn.Name = "chn2RadioBtn";
            this.chn2RadioBtn.Size = new System.Drawing.Size(31, 17);
            this.chn2RadioBtn.TabIndex = 1;
            this.chn2RadioBtn.TabStop = true;
            this.chn2RadioBtn.Tag = "Channel2";
            this.chn2RadioBtn.Text = "2";
            this.chn2RadioBtn.UseVisualStyleBackColor = true;
            // 
            // chn1RadioBtn
            // 
            this.chn1RadioBtn.AutoSize = true;
            this.chn1RadioBtn.Location = new System.Drawing.Point(44, 13);
            this.chn1RadioBtn.Name = "chn1RadioBtn";
            this.chn1RadioBtn.Size = new System.Drawing.Size(31, 17);
            this.chn1RadioBtn.TabIndex = 0;
            this.chn1RadioBtn.TabStop = true;
            this.chn1RadioBtn.Tag = "Channel1";
            this.chn1RadioBtn.Text = "1";
            this.chn1RadioBtn.UseVisualStyleBackColor = true;
            this.chn1RadioBtn.CheckedChanged += new System.EventHandler(this.OnSelectedFilterTypeOrChannelChanged);
            // 
            // FilterSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 277);
            this.Controls.Add(this.channelsSetGroup);
            this.Controls.Add(this.filterOrderTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.calculateBtn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.winRateTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.upFreqTextBox);
            this.Controls.Add(this.downFreqTextBox);
            this.Controls.Add(this.filterTypeSelector);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximumSize = new System.Drawing.Size(738, 316);
            this.Name = "FilterSettingsForm";
            this.Text = "Настройки фильтра";
            this.panel1.ResumeLayout(false);
            this.channelsSetGroup.ResumeLayout(false);
            this.channelsSetGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl filterPreviewGraph;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox filterTypeSelector;
        private System.Windows.Forms.TextBox downFreqTextBox;
        private System.Windows.Forms.TextBox upFreqTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button calculateBtn;
        private System.Windows.Forms.TextBox winRateTextBox;
        private System.Windows.Forms.ComboBox scaleTypeSetter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox filterOrderTextBox;
        private System.Windows.Forms.GroupBox channelsSetGroup;
        private System.Windows.Forms.RadioButton chn2RadioBtn;
        private System.Windows.Forms.RadioButton chn1RadioBtn;
    }
}