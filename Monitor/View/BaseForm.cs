﻿using System;
using System.Windows.Forms;

namespace Monitor.View
{
    public class BaseForm : Form
    {
        protected static void RethrowAction(Action act)
        {
            if (act != null)
                act();
        }

        protected static void RethrowAction<TArg>(Action<TArg> act, TArg arg)
        {
            if (act != null)
                act(arg);
        }

        protected static void RethrowAction<TArg1, TArg2>(Action<TArg1, TArg2> act, TArg1 arg1, TArg2 arg2)
        {
            if (act != null)
                act(arg1, arg2);
        }

        protected static void TryInvoke(Control element, Action action)
        {
            if (element.InvokeRequired)
            {
                element.Invoke(action, null);
            }
            else
            {
                action();
            }
        }

        protected static T TryInvoke<T>(Control element, Func<T> func)
        {
            if (element.InvokeRequired)
            {
                return (T)element.Invoke(func, null);
            }
            else
            {
                return func();
            }
        }
    }
}
