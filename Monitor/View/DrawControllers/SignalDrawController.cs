﻿using System;
using System.Drawing;
using ZedGraph;

using Monitor.View.Common;
using Monitor.CommonTypes;

using AxisType = Monitor.CommonTypes.AxisType;

namespace Monitor.View.DrawControllers
{
    public class SignalDrawController : IDrawController
    {
        private const double TIME_FRAME = 0.004;
        private const int POINTS_COUNT = 2000;
        private const double MAX_TIME_IN_RANGE = 8.0;

        private double actualYMax;
        private double actualYMin;
        
        private int currentId = 0;
        private string _title;
        private ZedGraphControl _control;
        private GraphPane _pane;
        private RollingPointPairList _signal;
        private RollingPointPairList _currPoint;             

        public SignalDrawController(ZedGraphControl control, string title)
        {
            _control = control;
            _pane = control.GraphPane;
            _title = title;
            _signal = new RollingPointPairList(POINTS_COUNT);
            for(var i=0; i<POINTS_COUNT; i++)
            {
                _signal.Add(i*TIME_FRAME, 0);   //чтобы потом корректно рисовалось, без перескоков
            }
            
            _currPoint = new RollingPointPairList(2);
            
            PrepareGraph();
        }

        private void PrepareGraph()
        {
            DrawGrid();
            SetAxis();

            Refresh();
        }

        private void DrawGrid()
        {
            _pane.XAxis.MajorGrid.IsVisible = true;
            _pane.XAxis.MajorGrid.DashOn = 15;
            _pane.XAxis.MajorGrid.DashOff = 10;

            _pane.XAxis.MinorGrid.IsVisible = true;
            _pane.XAxis.MinorGrid.DashOn = 1;
            _pane.XAxis.MinorGrid.DashOff = 2;


            _pane.YAxis.MajorGrid.IsVisible = true;
            _pane.YAxis.MajorGrid.DashOn = 8;
            _pane.YAxis.MajorGrid.DashOff = 3;

            _pane.YAxis.MinorGrid.IsVisible = true;
            _pane.YAxis.MinorGrid.DashOn = 1;
            _pane.YAxis.MinorGrid.DashOff = 2;
        }

        private void SetAxis()
        {            
            _pane.Title.Text = _title;
            _pane.IsBoundedRanges = true;

            actualYMax = 0;
            actualYMin = 0;
            _pane.YAxis.Scale.MinAuto = true;
            _pane.YAxis.Scale.MaxAuto = true;
            _pane.YAxis.Title.Text = "U, mV";
            _pane.YAxis.Title.FontSpec.Angle = 90;
            

            _pane.XAxis.Scale.Max = 8;
            _pane.XAxis.Scale.IsVisible = false;
            _pane.XAxis.Title.Text = "";
        }

        #region реализация интерфейса IDrawController

        public void AddDataSample(double[] sample)
        {  
            _signal[currentId].X = currentId * TIME_FRAME;
            _signal[currentId].Y = sample[0];


            if (sample[0] < actualYMin)
            {
                actualYMin = sample[0];
                
            }
            if(actualYMax < sample[0])
            {
                actualYMax = sample[0];
            }

            if (currentId == POINTS_COUNT-1)
            {
                actualYMin = sample[0];
                actualYMax = sample[0];
            }

            _currPoint.Add(currentId * TIME_FRAME, actualYMax);
            _currPoint.Add(currentId * TIME_FRAME, actualYMin);

            currentId = ++currentId % POINTS_COUNT;
        }
        
        public void Refresh()
        {            
            _pane.CurveList.Clear();
            
            _pane.AddCurve("", _signal, Color.Blue, SymbolType.None);
            var line = _pane.AddCurve("", _currPoint, Color.ForestGreen, SymbolType.None);
            line.Line.Width = 4;
            
            _control.AxisChange();            
            _control.Invalidate();
        }
        

        public Range ActualXRange
        {
            get
            {                
                return new Range { Min = _pane.XAxis.Scale.Min, Max = _pane.XAxis.Scale.Max };
            }

            set
            {                
                _pane.XAxis.Scale.Min = (value.Min < 0) ? 0 : value.Min;
                _pane.XAxis.Scale.Max = (value.Max > MAX_TIME_IN_RANGE) ? MAX_TIME_IN_RANGE : value.Max;

                Refresh();
            }
        }

        public Range ActualYRange
        {
            get
            {                
                return new Range { Min = _pane.YAxis.Scale.Min, Max = _pane.YAxis.Scale.Max };
            }

            set
            {               
                _pane.YAxis.Scale.Min = (value.Min < -7) ? -7 : value.Min;
                _pane.YAxis.Scale.Max = (value.Max > 7) ? 7 : value.Max;

                Refresh();
            }
        }

        public AxisType TypeOfAxis
        {
            get
            {                
                return (AxisType)_pane.YAxis.Type;
            }
            set
            {                
                _pane.YAxis.Type = (ZedGraph.AxisType)value;

                Refresh();
            }
        }

        public string TitleOfXAxis
        {
            get
            {
                return _pane.XAxis.Title.Text;
            }
            set
            {
                _pane.XAxis.Title.Text = value;
            }
        }

        public string TitleOfYAxis
        {
            get
            {
                return _pane.YAxis.Title.Text;
            }
            set
            {
                _pane.YAxis.Title.Text = value;
            }
        }

        public int PointsCount
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion     
    }
}