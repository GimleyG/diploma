﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ZedGraph;
using Monitor.View.Common;
using System.Drawing;
using Monitor.CommonTypes;

using AxisType = Monitor.CommonTypes.AxisType;

namespace Monitor.View.DrawControllers
{
    class FilterPreviewDrawController : IDrawController
    {
        private double FREQ_FRAME = 250.0 / 2048;
        private int POINTS_COUNT = 2048;
        private const double MAX_FREQ_IN_RANGE = 125.0;
                
        private ZedGraphControl _control;
        private GraphPane _pane;
        private RollingPointPairList _values;

        public FilterPreviewDrawController(ZedGraphControl control)
        {
            _control = control;
            _pane = control.GraphPane;
            _values = new RollingPointPairList(POINTS_COUNT);
            for (var i = 0; i < POINTS_COUNT; i++)
            {
                _values.Add(i * FREQ_FRAME, 0);
            }

            PrepareGraph();
        }

        private void PrepareGraph()
        {
            DrawGrid();
            SetAxis();

            Refresh();
        }

        private void DrawGrid()
        {
            _pane.XAxis.MajorGrid.IsVisible = true;
            _pane.XAxis.MajorGrid.DashOn = 15;
            _pane.XAxis.MajorGrid.DashOff = 10;

            _pane.XAxis.MinorGrid.IsVisible = true;
            _pane.XAxis.MinorGrid.DashOn = 1;
            _pane.XAxis.MinorGrid.DashOff = 2;


            _pane.YAxis.MajorGrid.IsVisible = true;
            _pane.YAxis.MajorGrid.DashOn = 8;
            _pane.YAxis.MajorGrid.DashOff = 3;

            _pane.YAxis.MinorGrid.IsVisible = true;
            _pane.YAxis.MinorGrid.DashOn = 1;
            _pane.YAxis.MinorGrid.DashOff = 2;
        }

        private void SetAxis()
        {
            _pane.Title.Text = "";

            _pane.YAxis.Scale.MinAuto = true;
            _pane.YAxis.Scale.MaxAuto = true;
            _pane.YAxis.Title.Text = "U, mV";
            _pane.YAxis.Title.FontSpec.Angle = 90;

            _pane.XAxis.Scale.Max = MAX_FREQ_IN_RANGE;
            _pane.XAxis.Scale.IsVisible = true;
            _pane.XAxis.Title.Text = "f, Hz";
        }

        #region Реализация интерфейса IDrawController

        public void AddDataSample(double[] sample)
        {
            for (var i = 0; i < POINTS_COUNT; i++)
            {
                _values[i].X = i * FREQ_FRAME;
                _values[i].Y = sample[i];
            }
        }

        public void Refresh()
        {
            var pane = _control.GraphPane;
            pane.CurveList.Clear();

            var line = pane.AddCurve("", _values, Color.Blue, SymbolType.None);
            line.Line.Width = 2;
            
            _control.AxisChange();            
            _control.Invalidate();
        }

        public Range ActualXRange
        {
            get
            {                
                return new Range { Min = _pane.XAxis.Scale.Min, Max = _pane.XAxis.Scale.Max };
            }

            set
            {
                _pane.XAxis.Scale.Min = (value.Min < 0) ? 0 : value.Min;
                _pane.XAxis.Scale.Max = (value.Max > MAX_FREQ_IN_RANGE) ? MAX_FREQ_IN_RANGE : value.Max;

                Refresh();
            }
        }

        public Range ActualYRange
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public AxisType TypeOfAxis
        {
            get
            {
                return (AxisType)_pane.YAxis.Type;
            }
            set
            {
                _pane.YAxis.Type = (ZedGraph.AxisType)value;
                    
                Refresh();
            }
        }
        
        public string TitleOfXAxis
        {
            get
            {
                return _pane.XAxis.Title.Text;
            }
            set
            {
                _pane.XAxis.Title.Text = value;
            }
        }

        public string TitleOfYAxis
        {
            get
            {
                return _pane.YAxis.Title.Text;
            }
            set
            {
                _pane.YAxis.Title.Text = value;
            }
        }

        public int PointsCount
        {
            get
            {
                return POINTS_COUNT;
            }
            set
            {
                POINTS_COUNT = value;
                FREQ_FRAME = 250.0 / POINTS_COUNT;

                _values = new RollingPointPairList(POINTS_COUNT);
                for (var i = 0; i < POINTS_COUNT; i++)
                {
                    _values.Add(i * FREQ_FRAME, 0);
                }

                Refresh();
            }
        }
        #endregion


        
    }
}
