﻿using System;
using System.Drawing;
using Monitor.View.Common;
using ZedGraph;

using AxisType = Monitor.CommonTypes.AxisType;
using Monitor.CommonTypes;

namespace Monitor.View.DrawControllers
{
    class SpectrumDrawController : IDrawController
    {
        private const double FREQ_FRAME = 250.0/4096;
        private const int POINTS_COUNT = 2048;
        private const double MAX_FREQ_IN_RANGE = 125.0;
        
        private ZedGraphControl _control;
        private GraphPane _pane;
        private RollingPointPairList values;

        bool isLog;

        public SpectrumDrawController(ZedGraphControl control)
        {
            _control = control;
            _pane = control.GraphPane;
            values = new RollingPointPairList(POINTS_COUNT);
            for(var i=0; i<POINTS_COUNT; i++)
            {
                values.Add(i * FREQ_FRAME, 0);   
            }
            isLog = false;
            PrepareGraph();
        }

        private void PrepareGraph()
        {
            DrawGrid();
            SetAxis();

            Refresh();
        }

        private void DrawGrid()
        {          
            _pane.XAxis.MajorGrid.IsVisible = true;
            _pane.XAxis.MajorGrid.DashOn = 15;
            _pane.XAxis.MajorGrid.DashOff = 10;

            _pane.XAxis.MinorGrid.IsVisible = true;
            _pane.XAxis.MinorGrid.DashOn = 1;
            _pane.XAxis.MinorGrid.DashOff = 2;


            _pane.YAxis.MajorGrid.IsVisible = true;
            _pane.YAxis.MajorGrid.DashOn = 8;
            _pane.YAxis.MajorGrid.DashOff = 3;

            _pane.YAxis.MinorGrid.IsVisible = true;
            _pane.YAxis.MinorGrid.DashOn = 1;
            _pane.YAxis.MinorGrid.DashOff = 2;
        }

        private void SetAxis()
        {           
            _pane.Title.Text = "";
            _pane.IsBoundedRanges = true;
            
            _pane.YAxis.Scale.MinAuto = true;
            _pane.YAxis.Scale.MaxAuto = true;
            _pane.YAxis.Title.Text = "U, mV";
            _pane.YAxis.Title.FontSpec.Angle = 90;
            
            _pane.XAxis.Scale.Max = MAX_FREQ_IN_RANGE;
            _pane.XAxis.Scale.IsVisible = true;
            _pane.XAxis.Title.Text = "f, Hz";
        }


        #region реализация интерфейса IDrawController

        public void AddDataSample(double[] sample)
        {
            double yval;
            
            for(var i=0; i < POINTS_COUNT; i++)
            {
                values[i].X = i*FREQ_FRAME;
                yval = (isLog) ? 20*Math.Log10(sample[i]) : sample[i];
                values[i].Y = yval;
            }            
        }       

        public void Refresh()
        {
            var _pane = _control.GraphPane;
            _pane.CurveList.Clear();
            var line = _pane.AddCurve("", values, Color.Green, SymbolType.None);
            line.Line.Width = 2;
            
            _control.AxisChange();
            _control.Invalidate();
        }

        public Range ActualXRange
        {
            get
            {                
                return new Range { Min = _pane.XAxis.Scale.Min, Max = _pane.XAxis.Scale.Max };
            }

            set
            {               
                _pane.XAxis.Scale.Min = (value.Min < 0) ? 0 : value.Min;
                _pane.XAxis.Scale.Max = (value.Max > MAX_FREQ_IN_RANGE) ? MAX_FREQ_IN_RANGE : value.Max;

                Refresh();
            }
        }

        public Range ActualYRange
        {
            get
            {
                return new Range { Min = _pane.YAxis.Scale.Min, Max = _pane.YAxis.Scale.Max };
            }

            set
            {
                _pane.YAxis.Scale.Min = (value.Min < -5) ? -5 : value.Min;
                _pane.YAxis.Scale.Max = (value.Max > 7) ? 7 : value.Max;

                Refresh();
            }
        }

        public AxisType TypeOfAxis
        {
            get
            {
                return (AxisType)_pane.YAxis.Type;
            }
            set
            {
                if (value == AxisType.LogDb)
                {
                    isLog = true;
                    _pane.YAxis.Type = ZedGraph.AxisType.Linear;
                }
                else
                {
                    _pane.YAxis.Type = (ZedGraph.AxisType)value;
                    isLog = false;
                }

                Refresh();                
            }
        }
        public string TitleOfXAxis
        {
            get
            {
                return _pane.XAxis.Title.Text;
            }
            set
            {
                _pane.XAxis.Title.Text = value;
            }
        }

        public string TitleOfYAxis
        {
            get
            {
                return _pane.YAxis.Title.Text;
            }
            set
            {
                _pane.YAxis.Title.Text = value;
            }
        }
        
        public int PointsCount
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}
