﻿using Monitor.CommonTypes;

namespace Monitor.View.Common
{
    public interface IDrawController
    {
        void AddDataSample(double[] sample);
        void Refresh();

        Range ActualXRange { get; set; }
        Range ActualYRange { get; set; }

        string TitleOfXAxis { get; set; }
        string TitleOfYAxis { get; set; }

        AxisType TypeOfAxis { get; set; }
        int PointsCount { get; set; }
    }
}