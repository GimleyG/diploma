﻿using Monitor.CommonTypes;
using System;
using System.Windows.Forms;

namespace Monitor.View.Common
{
    public interface IMainView : IView
    {
        //device
        event Action DeviceConnectionPerformed;
        void AddDeviceDescription(string item);
        void AddDeviceSpeed(uint speed);
        void ClearDeviceInfo(string item);
        string ConnectionBtnCapture { set; }
        string SelectedDevice { get; }
        uint SelectedDeviceSpeed { get; }

        //main view
        event Action DeviceListChanged;
        event Action Closing;
        bool SignalTimerSwitchOn { set; }
        bool SpectrumTimerSwitchOn { set; }      
        string StatusLabel { get; set; }
        string FilterStatusLabel { get; set; }

        //graph
        event Action<GraphType, ZoomType> GraphZooming;  
          //signal
        IDrawController SignalChannel1Graph { get; }
        IDrawController SignalChannel2Graph { get; }
        SignalSource SignalChannel1Source { get; set; }
        SignalSource SignalChannel2Source { get; set; }
        event Action SignalTimerTick;
        void ChangeLedFor(bool turnOn, FilterType type, Channel chn);
        void ChangeLedTipFor(string value, FilterType type, Channel chn);
           
          //spectrum
        event Action SpectrumNavigationBarScroll;
        event Action<AxisType> SpectrumAxisTypeChanged;
        event Action SpectrumTimerTick;
        IDrawController SpectrumGraph { get; }
        Range SpectrumNavigationBarRange { get; set; }
        bool SpectrumNavigationBarVisible { get; set; }
        WindowType SelectedSpectrumWinType { get; set; }
        Channel SelectedSpectrumChannel { get; set; }
        SignalSource SpectrumSource { get; set; }

        //filter
        event Action<FilterType, Channel> FilterActivityChanged;
        event Action FilterSettingsInvoked;
        event Action MiddleFilterTypeChanged;
        FilterType MiddleFilterType {get; set;}
    }
}
