﻿using System;
using System.Windows.Forms;

namespace Monitor.View.Common
{
    public interface IView
    {
        void Show();
        void Close();

        DialogResult ShowMessage(string msg, string caption, MessageBoxIcon icon);
        DialogResult ShowMessage(string msg, string caption, MessageBoxIcon icon, MessageBoxButtons btns);
        Cursor Cursors { get; set; }
    }    
}