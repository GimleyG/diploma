﻿using System;
using System.Windows.Forms;

using Monitor.CommonTypes;

namespace Monitor.View.Common
{
    public interface IFilterSettingsView : IView
    {
        event Action ConfirmInvoked;
        event Action CalculateInvoked;
        event Action<AxisType> SelectedAxisTypeChanged;
        event Action<FilterType, Channel> SelectedFilterTypeOrChannelChanged;

        Channel SelectedChannel { get; set; }
        FilterType SelectedFilterType { get; set; }
        int FilterOrder { get; set; }
        Range FrequencyRange { get; set; }
        double WindowRate { get; set; }
        bool LowRangeAvailability{ get; set; }
        IDrawController FilterPreviewGraph { get; }
    }
}