﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

using Monitor.View.Common;
using Monitor.View.DrawControllers;
using Monitor.CommonTypes;
using System.Threading;

namespace Monitor.View
{
    
    public partial class MainForm : BaseForm, IMainView
    {
        private SignalDrawController _signalChn1Controller;
        private SignalDrawController _signalChn2Controller;
        private SpectrumDrawController _spectrumController;

        private SignalSource _actualSignalChn1Source;
        private SignalSource _actualSignalChn2Source;
        private SignalSource _actualSpectrumSource;
        private Channel _actualSpectrumChannel;
        private WindowType _actualSpectrumWinType;
        private FilterType _actualMiddleFilterType;

        private readonly ApplicationContext _context;
        public MainForm(ApplicationContext context)
        {
            _context = context;

            InitializeComponent();

            _signalChn1Controller = new SignalDrawController(signalChn1Graph, "Channel 1");
            _signalChn2Controller = new SignalDrawController(signalChn2Graph, "Channel 2");
            _spectrumController = new SpectrumDrawController(spectrumGraph);
            spectrumScaleSetter.SelectedIndex = 0;
        }

        private const int WM_DeviceChange = 0x219; //событие подключения устройства usb
        private const int DBT_DEVNODES_CHANGED = 0x0007; //устройство подключено или отключено

        protected override void WndProc(ref Message msg)
        {
            base.WndProc(ref msg);            
            
            if (msg.Msg == WM_DeviceChange)
            {
                int wParam = msg.WParam.ToInt32();
                if ( wParam == DBT_DEVNODES_CHANGED)
                {                    
                    OnDeviceListChanged();
                    Thread.Sleep(200);
                }                
            }
        }

        #region Обработчики компонентов формы
        
        private void OnConnectDeviceBtnClicked(object sender, EventArgs arg)
        {
            RethrowAction(DeviceConnectionPerformed);
        }

        private void OnSignalTimerTick(object sender, EventArgs arg)
        {
            RethrowAction(SignalTimerTick);
        }

        private void OnSpectrumTimerTick(object sender, EventArgs arg)
        {
            RethrowAction(SpectrumTimerTick);
        }
        
        private void OnDeviceListChanged()
        {
            RethrowAction(DeviceListChanged);
        }

        private void OnFilterSettingsInvoked()
        {
            RethrowAction(FilterSettingsInvoked);
        }

        private void OnFormClosing(object sender, FormClosingEventArgs arg)
        {
            RethrowAction(Closing);
        }

        private void OnGraphZoomBtn_Click(object sender, EventArgs e)
        {
            if (GraphZooming != null)
            {
                var btn = sender as ToolStripButton;
                switch ((string)btn.Tag)
                {
                    case "ZoomInSpectrum":
                        GraphZooming(GraphType.Spectrum, ZoomType.In);
                        break;
                    case "ZoomOutSpectrum":
                        GraphZooming(GraphType.Spectrum, ZoomType.Out);
                        break;
                    case "ZoomInChn1":
                        GraphZooming(GraphType.SignalChannel1, ZoomType.In);
                        break;
                    case "ZoomOutChn1":
                        GraphZooming(GraphType.SignalChannel1, ZoomType.Out);
                        break;
                    case "ZoomInChn2":
                        GraphZooming(GraphType.SignalChannel2, ZoomType.In);
                        break;
                    case "ZoomOutChn2":
                        GraphZooming(GraphType.SignalChannel2, ZoomType.Out);
                        break;
                }
            }
        }

        private void OnSpectrumNavigationBar_Scroll(object sender, ScrollEventArgs e)
        {
            RethrowAction(SpectrumNavigationBarScroll);
        }

        private void OnSpectrumAxisTypeChanged(object sender, EventArgs e)
        {
            if (SpectrumAxisTypeChanged != null)
            {
                SpectrumAxisTypeChanged((AxisType)spectrumScaleSetter.SelectedIndex);
            }
        } 

        #endregion

        #region Обработчики пунктов меню

        //in File
        private void fileStripMenuItem_Click(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            switch ((string)menuItem.Tag)
            {
                case "quit":
                    Close();
                    break;
            }
        }

        //in View
        private void viewStripMenuItem_Click(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            bool visible = menuItem.Checked;
            var upPanelHidden = true;

            switch ((string) menuItem.Tag)
            {
                case "portSets":
                    deviceSetsPanel.Visible = visible;
                    upPanelHidden = !visible;
                    break;
                case "spectrum":
                    spectrumPanel.Visible = visible;
                    mainContainer.Panel2Collapsed = !visible;
                    break;
                case "channel1":                    
                    signalChn1Panel.Visible = visible;        
                    signalGraphsContainer.Panel1Collapsed = upPanelHidden = !visible;
                    signalGraphsContainer.Panel2Collapsed = !signalChn2Panel.Visible;
                    break;
                case "channel2":
                    signalChn2Panel.Visible = visible;
                    signalGraphsContainer.Panel2Collapsed = upPanelHidden = !visible;
                    signalGraphsContainer.Panel1Collapsed = !signalChn1Panel.Visible;
                    break;
            }

            mainContainer.Panel1Collapsed = !(deviceSetsPanel.Visible ||  signalChn1Panel.Visible || signalChn2Panel.Visible)  && upPanelHidden;
            
        }

        //in Filter
        private void filterStripMenuItem_Click(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
                     
            switch ((string)menuItem.Tag)
            {                
                case "settings":
                    OnFilterSettingsInvoked();                    
                    break;                
            }
        }
        private void OnMiddleFilterMnuItm_Click(object sender, EventArgs e)
        {
            var mnuItm = sender as ToolStripMenuItem;
            if ((string)mnuItm.Tag == "passband")
                middleFilterLowpassMnuItm.Checked = !middleFilterPassbandMnuItm.Checked;
            else
                middleFilterPassbandMnuItm.Checked = !middleFilterLowpassMnuItm.Checked;
        }
        private void OnFilterActiveStateChanged(object sender, EventArgs e)
        {
            if (FilterActivityChanged != null)
            {
                var mnuItm = sender as ToolStripMenuItem;

                switch ((string)mnuItm.Tag)
                {
                    case "MiddleFilterChn1":
                        FilterActivityChanged(_actualMiddleFilterType, Channel.Chn1);
                        break;
                    case "MiddleFilterChn2":
                        FilterActivityChanged(_actualMiddleFilterType, Channel.Chn2);
                        break;
                    case "StopbandChn1":
                        FilterActivityChanged(FilterType.Stopband, Channel.Chn1);
                        break;
                    case "StopbandChn2":
                        FilterActivityChanged(FilterType.Stopband, Channel.Chn2);
                        break;
                }
            }
        }
        private void OnMiddleFilterType_CheckedChanged(object sender, EventArgs e)
        {
            if (middleFilterPassbandMnuItm.Checked)
                _actualMiddleFilterType = FilterType.Passband;
            else
                _actualMiddleFilterType = FilterType.Lowpass;

            RethrowAction(MiddleFilterTypeChanged);
        }
        #endregion

        #region Обработчики пунктов панели инструментов

        private void panelHideBtn_Click(object sender, EventArgs e)
        {
            var hideBtn = sender as ToolStripButton;
            switch ((string) hideBtn.Tag)
            {
                case "spectrumHide":
                    spectrumPanel.Hide();
                    spectrumToolStripMenuItem.Checked = false;
                    mainContainer.Panel2Collapsed = true;
                    break;
                case "signalChn1Hide":
                    signalChn1Panel.Hide();
                    signalGraphsContainer.Panel1Collapsed = true;
                    chn1ToolStripMenuItem.Checked = false;
                    break;
                case "signalChn2Hide":
                    signalChn2Panel.Hide();
                    signalGraphsContainer.Panel2Collapsed = true;
                    chn2ToolStripMenuItem.Checked = false;
                    break;
                case "deviceSetsHide":
                    deviceSetsPanel.Hide();
                    portSettingsToolStripMenuItem.Checked = false;
                    break;
            }
            mainContainer.Panel1Collapsed = !(deviceSetsPanel.Visible || signalChn1Panel.Visible || signalChn2Panel.Visible);
        }

        #endregion
                
        #region Реализация IMainView

        void IView.Show()
        {
            _context.MainForm = this;
            Application.Run(_context);
        }
        void IView.Close()
        {
            this.Close();
        }

        //device
        public event Action DeviceConnectionPerformed;
        public void AddDeviceDescription(string item)
        {
            TryInvoke(deviceComboBox, () =>
            {
                deviceComboBox.Items.Add(item);
                deviceComboBox.Enabled = true;
                connectBtn.Enabled = true;
            });
        }
        public void AddDeviceSpeed(uint speed)
        {
            TryInvoke(deviceSpeedComboBox, () =>
            {
                if(!deviceSpeedComboBox.Items.Contains(speed))
                    deviceSpeedComboBox.Items.Add(speed);
                deviceSpeedComboBox.Enabled = true;
            });
        }
        public string ConnectionBtnCapture
        {
            set
            {
                TryInvoke(connectBtn, () => connectBtn.Text = value);
            }
        }
        public void ClearDeviceInfo(string item)
        {
            TryInvoke(deviceComboBox, () =>
                                        {
                                            deviceComboBox.Items.Remove(item);
                                            if (deviceComboBox.Items.Count == 0)
                                            {
                                                deviceComboBox.Enabled = false;
                                                connectBtn.Enabled = false;
                                            }
                                        } );
            TryInvoke(deviceSpeedComboBox, () =>
                                        {
                                            if (deviceComboBox.Items.Count == 0)
                                            {
                                                deviceSpeedComboBox.Items.Clear();
                                                deviceSpeedComboBox.Enabled = false;
                                            }
                                        });
        }
        public string SelectedDevice
        {
            get { return TryInvoke<string>(deviceComboBox, () => (string)deviceComboBox.SelectedItem); }
        }
        public uint SelectedDeviceSpeed
        {
            get { return TryInvoke<uint>(deviceSpeedComboBox, () => (uint)deviceSpeedComboBox.SelectedItem); }
        }

        //main view        
        public event Action DeviceListChanged;
        public new event Action Closing;
        public bool SignalTimerSwitchOn
        {
            set{ signalTimer.Enabled = value; }
        }
        public bool SpectrumTimerSwitchOn
        {
            set { spectrumTimer.Enabled = value; }
        }
        public DialogResult ShowMessage(string msg, string caption, MessageBoxIcon icon)
        {
            return MessageBox.Show(msg, caption, MessageBoxButtons.OK, icon);
        }
        public DialogResult ShowMessage(string msg, string caption, MessageBoxIcon icon, MessageBoxButtons btns)
        {
            return MessageBox.Show(msg, caption, btns, icon);
        }
        public Cursor Cursors
        {
            get { return this.Cursor; }
            set
            {
                TryInvoke(this, () => Cursor = value);
            }
        }
        public string StatusLabel
        {
            get
            {
                return statusLabel.Text;
            }
            set
            {
                statusLabel.Text = value;
            }
        }
        public string FilterStatusLabel
        {
            get
            {
                return filterStatusLabel.Text;
            }
            set
            {
                filterStatusLabel.Text = value;
            }
        }
        
        
        //graph
        public event Action SignalTimerTick;
        public event Action SpectrumTimerTick;
        public event Action<GraphType, ZoomType> GraphZooming;
        public event Action SpectrumNavigationBarScroll;
        public event Action<AxisType> SpectrumAxisTypeChanged;
        public IDrawController SignalChannel1Graph
        {
            get { return _signalChn1Controller; }
        }
        public IDrawController SignalChannel2Graph
        {
            get { return _signalChn2Controller; }
        }
        public IDrawController SpectrumGraph
        {
            get { return _spectrumController; }
        }
        public Range SpectrumNavigationBarRange
        {
            get 
            {
                var width = spectrumNavigationScrollBar.LargeChange;
                var min = spectrumNavigationScrollBar.Value;
                return new Range { Min = min, Max = min + width };
            }
            set 
            {
                spectrumNavigationScrollBar.LargeChange = (int)Math.Round(value.Width);
                spectrumNavigationScrollBar.Value = (int)Math.Round(value.Min);
            }
        }
        public bool SpectrumNavigationBarVisible
        {
            get
            {
                return spectrumNavigationScrollBar.Visible;
            }
            set
            {
                spectrumNavigationScrollBar.Visible = value;
            }
        }
        public WindowType SelectedSpectrumWinType
        {
            get
            {
                return _actualSpectrumWinType;
            }
            set
            {
                TryInvoke(spectrumWinSetter.GetCurrentParent(), () =>
                                                    {
                                                        spectrumWinSetter.SelectedIndex = (int)value;
                                                    });                
            }
        }
        public Channel SelectedSpectrumChannel
        {
            get
            {
                return _actualSpectrumChannel;
            }
            set
            {
                TryInvoke(spectrumChnSetter.GetCurrentParent(), () =>
                {
                    spectrumChnSetter.SelectedIndex = (int)value;
                });
            }
        }
        public SignalSource SignalChannel1Source
        {
            get
            {
                return _actualSignalChn1Source;
            }
            set
            {
                TryInvoke(signalChn1Source.GetCurrentParent(), () =>
                {
                    signalChn1Source.SelectedIndex = (int)value;
                });
            }
        }
        public SignalSource SignalChannel2Source
        {
            get
            {
                return _actualSignalChn2Source;
            }
            set
            {
                TryInvoke(signalChn2Source.GetCurrentParent(), () =>
                {
                    signalChn2Source.SelectedIndex = (int)value;
                });
            }
        }
        public SignalSource SpectrumSource
        {
            get
            {
                return _actualSpectrumSource;
            }
            set
            {
                TryInvoke(spectrumSource.GetCurrentParent(), () =>
                {
                    spectrumSource.SelectedIndex = (int)value;
                });
            }
        }
        public void ChangeLedFor(bool turnOn, FilterType type, Channel chn)
        {
            Action<Bitmap, FilterType, Channel> setImg = (Bitmap bitMap, FilterType t, Channel ch) =>
                                        {
                                            if (type == FilterType.Stopband)
                                            {
                                                if (chn == Channel.Chn1)
                                                    stopbandFilterChn1ActivityLed.Image = bitMap;
                                                else
                                                    stopbandFilterChn2ActivityLed.Image = bitMap;
                                            }
                                            else
                                            {
                                                if (chn == Channel.Chn1)
                                                    middleFilterChn1ActivityLed.Image = bitMap;
                                                else
                                                    middleFilterChn2ActivityLed.Image = bitMap;
                                            }
                                        };
            if (turnOn)
            {
                setImg(Properties.Resources.greenLed, type, chn);
            }
            else
            {
                setImg(Properties.Resources.redLed, type, chn);
            }
        }
        public void ChangeLedTipFor(string value, FilterType type, Channel chn)
        {
            if (type == FilterType.Stopband)
            {
                if (chn == Channel.Chn1)
                {
                    stopbandFilterChn1ActivityLed.ToolTipText = value;
                }
                else
                {
                    stopbandFilterChn2ActivityLed.ToolTipText = value;                
                }
            }
            else
            {
                if (chn == Channel.Chn1)
                { 
                    middleFilterChn1ActivityLed.ToolTipText = value;
                }
                else
                {
                    middleFilterChn2ActivityLed.ToolTipText = value;
                }
            }
        }

        //filter
        public event Action<FilterType, Channel> FilterActivityChanged;
        public event Action FilterSettingsInvoked;
        public event Action MiddleFilterTypeChanged;
        public FilterType MiddleFilterType
        {
            get
            {
                return _actualMiddleFilterType;
            }
            set
            {
                if (value == FilterType.Passband)
                {
                    middleFilterPassbandMnuItm.Checked = true;
                    middleFilterLowpassMnuItm.Checked = false;
                }
                else
                {
                    middleFilterPassbandMnuItm.Checked = false;
                    middleFilterLowpassMnuItm.Checked = true;
                }
            }
        }
      
        #endregion         

        private void OnSelectedItemChanged(object sender, EventArgs e)
        {
            var setter = sender as ToolStripComboBox;
            switch ((string)setter.Tag)
            { 
                case "SignalChn1Source":
                    _actualSignalChn1Source = (SignalSource)signalChn1Source.SelectedIndex;
                    break;
                case "SignalChn2Source":
                    _actualSignalChn2Source = (SignalSource)signalChn2Source.SelectedIndex;
                    break;
                case "SpectrumSource":
                    _actualSpectrumSource = (SignalSource)spectrumSource.SelectedIndex;
                    break;
                case "SpectrumWinSetter":
                    _actualSpectrumWinType = (WindowType)spectrumWinSetter.SelectedIndex;
                    break;
                case "SpectrumChannelChanged":
                    _actualSpectrumChannel = (Channel)spectrumChnSetter.SelectedIndex;
                    break;                     
            }
        }

    }
}
