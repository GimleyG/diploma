﻿using System.Runtime.CompilerServices;
using System.Windows.Forms;
using ZedGraph;

namespace Monitor.View
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows
        
        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spectrumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chn1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chn2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleFilterTypeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleFilterPassbandMnuItm = new System.Windows.Forms.ToolStripMenuItem();
            this.middleFilterLowpassMnuItm = new System.Windows.Forms.ToolStripMenuItem();
            this.middleFilterActiveStateMenuItm = new System.Windows.Forms.ToolStripMenuItem();
            this.passbandChn1ActiveStateMnuItm = new System.Windows.Forms.ToolStripMenuItem();
            this.passbandChn2ActiveStateMnuItm = new System.Windows.Forms.ToolStripMenuItem();
            this.stopbandActiveStateMenuItm = new System.Windows.Forms.ToolStripMenuItem();
            this.stopbandChn1ActiveStateMnuItm = new System.Windows.Forms.ToolStripMenuItem();
            this.stopbandChn2ActiveStateMnuItm = new System.Windows.Forms.ToolStripMenuItem();
            this.filterSettingMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signalTimer = new System.Windows.Forms.Timer(this.components);
            this.statusLable = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.filterStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.spectrumPanel = new System.Windows.Forms.Panel();
            this.spectrumGraph = new ZedGraph.ZedGraphControl();
            this.spectrumNavigationScrollBar = new System.Windows.Forms.HScrollBar();
            this.topTools = new System.Windows.Forms.ToolStrip();
            this.spectrumHideBtn = new System.Windows.Forms.ToolStripButton();
            this.spectrumZoomInBtn = new System.Windows.Forms.ToolStripButton();
            this.spectrumZoomOutBtn = new System.Windows.Forms.ToolStripButton();
            this.spectrumScaleSetter = new System.Windows.Forms.ToolStripComboBox();
            this.spectrumChnSetter = new System.Windows.Forms.ToolStripComboBox();
            this.spectrumSource = new System.Windows.Forms.ToolStripComboBox();
            this.spectrumWinSetter = new System.Windows.Forms.ToolStripComboBox();
            this.deviceSetsPanel = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.deviceSetsHideBtn = new System.Windows.Forms.ToolStripButton();
            this.label2 = new System.Windows.Forms.Label();
            this.deviceSpeedComboBox = new System.Windows.Forms.ComboBox();
            this.connectBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.deviceComboBox = new System.Windows.Forms.ComboBox();
            this.spectrumTimer = new System.Windows.Forms.Timer(this.components);
            this.mainContainer = new System.Windows.Forms.SplitContainer();
            this.signalGraphsContainer = new System.Windows.Forms.SplitContainer();
            this.signalChn1Panel = new System.Windows.Forms.Panel();
            this.signalChn1Graph = new ZedGraph.ZedGraphControl();
            this.chn1Tools = new System.Windows.Forms.ToolStrip();
            this.signalChn1HideBtn = new System.Windows.Forms.ToolStripButton();
            this.signalChn1ZoomInBtn = new System.Windows.Forms.ToolStripButton();
            this.signalChn1ZoomOutBtn = new System.Windows.Forms.ToolStripButton();
            this.signalChn1Source = new System.Windows.Forms.ToolStripComboBox();
            this.middleFilterChn1ActivityLed = new System.Windows.Forms.ToolStripLabel();
            this.stopbandFilterChn1ActivityLed = new System.Windows.Forms.ToolStripLabel();
            this.signalChn2Panel = new System.Windows.Forms.Panel();
            this.signalChn2Graph = new ZedGraph.ZedGraphControl();
            this.chn2Tools = new System.Windows.Forms.ToolStrip();
            this.signalChn2HideBtn = new System.Windows.Forms.ToolStripButton();
            this.signalChn2ZoomInBtn = new System.Windows.Forms.ToolStripButton();
            this.signalChn2ZoomOutBtn = new System.Windows.Forms.ToolStripButton();
            this.signalChn2Source = new System.Windows.Forms.ToolStripComboBox();
            this.middleFilterChn2ActivityLed = new System.Windows.Forms.ToolStripLabel();
            this.stopbandFilterChn2ActivityLed = new System.Windows.Forms.ToolStripLabel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.menuStrip.SuspendLayout();
            this.statusLable.SuspendLayout();
            this.spectrumPanel.SuspendLayout();
            this.topTools.SuspendLayout();
            this.deviceSetsPanel.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainContainer)).BeginInit();
            this.mainContainer.Panel1.SuspendLayout();
            this.mainContainer.Panel2.SuspendLayout();
            this.mainContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.signalGraphsContainer)).BeginInit();
            this.signalGraphsContainer.Panel1.SuspendLayout();
            this.signalGraphsContainer.Panel2.SuspendLayout();
            this.signalGraphsContainer.SuspendLayout();
            this.signalChn1Panel.SuspendLayout();
            this.chn1Tools.SuspendLayout();
            this.signalChn2Panel.SuspendLayout();
            this.chn2Tools.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.filterToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1003, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.quitToolStripMenuItem.Tag = "quit";
            this.quitToolStripMenuItem.Text = "Выход";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.fileStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.portSettingsToolStripMenuItem,
            this.spectrumToolStripMenuItem,
            this.signalToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.viewToolStripMenuItem.Text = "Вид";
            // 
            // portSettingsToolStripMenuItem
            // 
            this.portSettingsToolStripMenuItem.Checked = true;
            this.portSettingsToolStripMenuItem.CheckOnClick = true;
            this.portSettingsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.portSettingsToolStripMenuItem.Name = "portSettingsToolStripMenuItem";
            this.portSettingsToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.portSettingsToolStripMenuItem.Tag = "portSets";
            this.portSettingsToolStripMenuItem.Text = "Подключение устройств";
            this.portSettingsToolStripMenuItem.Click += new System.EventHandler(this.viewStripMenuItem_Click);
            // 
            // spectrumToolStripMenuItem
            // 
            this.spectrumToolStripMenuItem.Checked = true;
            this.spectrumToolStripMenuItem.CheckOnClick = true;
            this.spectrumToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.spectrumToolStripMenuItem.Name = "spectrumToolStripMenuItem";
            this.spectrumToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.spectrumToolStripMenuItem.Tag = "spectrum";
            this.spectrumToolStripMenuItem.Text = "Спектр";
            this.spectrumToolStripMenuItem.Click += new System.EventHandler(this.viewStripMenuItem_Click);
            // 
            // signalToolStripMenuItem
            // 
            this.signalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chn1ToolStripMenuItem,
            this.chn2ToolStripMenuItem});
            this.signalToolStripMenuItem.Name = "signalToolStripMenuItem";
            this.signalToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.signalToolStripMenuItem.Tag = "signal";
            this.signalToolStripMenuItem.Text = "Осциллограф";
            // 
            // chn1ToolStripMenuItem
            // 
            this.chn1ToolStripMenuItem.Checked = true;
            this.chn1ToolStripMenuItem.CheckOnClick = true;
            this.chn1ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chn1ToolStripMenuItem.Name = "chn1ToolStripMenuItem";
            this.chn1ToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.chn1ToolStripMenuItem.Tag = "channel1";
            this.chn1ToolStripMenuItem.Text = "Канал 1";
            this.chn1ToolStripMenuItem.Click += new System.EventHandler(this.viewStripMenuItem_Click);
            // 
            // chn2ToolStripMenuItem
            // 
            this.chn2ToolStripMenuItem.Checked = true;
            this.chn2ToolStripMenuItem.CheckOnClick = true;
            this.chn2ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chn2ToolStripMenuItem.Name = "chn2ToolStripMenuItem";
            this.chn2ToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.chn2ToolStripMenuItem.Tag = "channel2";
            this.chn2ToolStripMenuItem.Text = "Канал 2";
            this.chn2ToolStripMenuItem.Click += new System.EventHandler(this.viewStripMenuItem_Click);
            // 
            // filterToolStripMenuItem
            // 
            this.filterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.middleFilterTypeMenuItem,
            this.middleFilterActiveStateMenuItm,
            this.stopbandActiveStateMenuItm,
            this.filterSettingMenuItem});
            this.filterToolStripMenuItem.Name = "filterToolStripMenuItem";
            this.filterToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.filterToolStripMenuItem.Text = "Фильтры";
            // 
            // middleFilterTypeMenuItem
            // 
            this.middleFilterTypeMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.middleFilterPassbandMnuItm,
            this.middleFilterLowpassMnuItm});
            this.middleFilterTypeMenuItem.Name = "middleFilterTypeMenuItem";
            this.middleFilterTypeMenuItem.Size = new System.Drawing.Size(269, 22);
            this.middleFilterTypeMenuItem.Text = "Тип основного фильтра";
            // 
            // middleFilterPassbandMnuItm
            // 
            this.middleFilterPassbandMnuItm.CheckOnClick = true;
            this.middleFilterPassbandMnuItm.Name = "middleFilterPassbandMnuItm";
            this.middleFilterPassbandMnuItm.Size = new System.Drawing.Size(157, 22);
            this.middleFilterPassbandMnuItm.Tag = "passband";
            this.middleFilterPassbandMnuItm.Text = "Полосовой";
            this.middleFilterPassbandMnuItm.CheckedChanged += new System.EventHandler(this.OnMiddleFilterType_CheckedChanged);
            this.middleFilterPassbandMnuItm.Click += new System.EventHandler(this.OnMiddleFilterMnuItm_Click);
            // 
            // middleFilterLowpassMnuItm
            // 
            this.middleFilterLowpassMnuItm.CheckOnClick = true;
            this.middleFilterLowpassMnuItm.Name = "middleFilterLowpassMnuItm";
            this.middleFilterLowpassMnuItm.Size = new System.Drawing.Size(157, 22);
            this.middleFilterLowpassMnuItm.Tag = "lowpass";
            this.middleFilterLowpassMnuItm.Text = "Нижних частот";
            this.middleFilterLowpassMnuItm.Click += new System.EventHandler(this.OnMiddleFilterMnuItm_Click);
            // 
            // middleFilterActiveStateMenuItm
            // 
            this.middleFilterActiveStateMenuItm.CheckOnClick = true;
            this.middleFilterActiveStateMenuItm.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.passbandChn1ActiveStateMnuItm,
            this.passbandChn2ActiveStateMnuItm});
            this.middleFilterActiveStateMenuItm.Name = "middleFilterActiveStateMenuItm";
            this.middleFilterActiveStateMenuItm.Size = new System.Drawing.Size(269, 22);
            this.middleFilterActiveStateMenuItm.Tag = "activeState";
            this.middleFilterActiveStateMenuItm.Text = "Активность основного фильтра";
            // 
            // passbandChn1ActiveStateMnuItm
            // 
            this.passbandChn1ActiveStateMnuItm.CheckOnClick = true;
            this.passbandChn1ActiveStateMnuItm.Name = "passbandChn1ActiveStateMnuItm";
            this.passbandChn1ActiveStateMnuItm.Size = new System.Drawing.Size(152, 22);
            this.passbandChn1ActiveStateMnuItm.Tag = "MiddleFilterChn1";
            this.passbandChn1ActiveStateMnuItm.Text = "Канал 1";
            this.passbandChn1ActiveStateMnuItm.CheckedChanged += new System.EventHandler(this.OnFilterActiveStateChanged);
            // 
            // passbandChn2ActiveStateMnuItm
            // 
            this.passbandChn2ActiveStateMnuItm.CheckOnClick = true;
            this.passbandChn2ActiveStateMnuItm.Name = "passbandChn2ActiveStateMnuItm";
            this.passbandChn2ActiveStateMnuItm.Size = new System.Drawing.Size(152, 22);
            this.passbandChn2ActiveStateMnuItm.Tag = "MiddleFilterChn2";
            this.passbandChn2ActiveStateMnuItm.Text = "Канал 2";
            this.passbandChn2ActiveStateMnuItm.CheckedChanged += new System.EventHandler(this.OnFilterActiveStateChanged);
            // 
            // stopbandActiveStateMenuItm
            // 
            this.stopbandActiveStateMenuItm.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stopbandChn1ActiveStateMnuItm,
            this.stopbandChn2ActiveStateMnuItm});
            this.stopbandActiveStateMenuItm.Name = "stopbandActiveStateMenuItm";
            this.stopbandActiveStateMenuItm.Size = new System.Drawing.Size(269, 22);
            this.stopbandActiveStateMenuItm.Text = "Активность режекторного фильтра";
            // 
            // stopbandChn1ActiveStateMnuItm
            // 
            this.stopbandChn1ActiveStateMnuItm.CheckOnClick = true;
            this.stopbandChn1ActiveStateMnuItm.Name = "stopbandChn1ActiveStateMnuItm";
            this.stopbandChn1ActiveStateMnuItm.Size = new System.Drawing.Size(116, 22);
            this.stopbandChn1ActiveStateMnuItm.Tag = "StopbandChn1";
            this.stopbandChn1ActiveStateMnuItm.Text = "Канал 1";
            this.stopbandChn1ActiveStateMnuItm.CheckedChanged += new System.EventHandler(this.OnFilterActiveStateChanged);
            // 
            // stopbandChn2ActiveStateMnuItm
            // 
            this.stopbandChn2ActiveStateMnuItm.CheckOnClick = true;
            this.stopbandChn2ActiveStateMnuItm.Name = "stopbandChn2ActiveStateMnuItm";
            this.stopbandChn2ActiveStateMnuItm.Size = new System.Drawing.Size(116, 22);
            this.stopbandChn2ActiveStateMnuItm.Tag = "StopbandChn2";
            this.stopbandChn2ActiveStateMnuItm.Text = "Канал 2";
            this.stopbandChn2ActiveStateMnuItm.CheckedChanged += new System.EventHandler(this.OnFilterActiveStateChanged);
            // 
            // filterSettingMenuItem
            // 
            this.filterSettingMenuItem.Name = "filterSettingMenuItem";
            this.filterSettingMenuItem.Size = new System.Drawing.Size(269, 22);
            this.filterSettingMenuItem.Tag = "settings";
            this.filterSettingMenuItem.Text = "Настройки";
            this.filterSettingMenuItem.Click += new System.EventHandler(this.filterStripMenuItem_Click);
            // 
            // signalTimer
            // 
            this.signalTimer.Interval = 10;
            this.signalTimer.Tick += new System.EventHandler(this.OnSignalTimerTick);
            // 
            // statusLable
            // 
            this.statusLable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.filterStatusLabel});
            this.statusLable.Location = new System.Drawing.Point(0, 432);
            this.statusLable.Name = "statusLable";
            this.statusLable.Size = new System.Drawing.Size(1003, 22);
            this.statusLable.TabIndex = 5;
            this.statusLable.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(172, 17);
            this.statusLabel.Text = "Нет подключенных устройств";
            // 
            // filterStatusLabel
            // 
            this.filterStatusLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.filterStatusLabel.Name = "filterStatusLabel";
            this.filterStatusLabel.Size = new System.Drawing.Size(110, 17);
            this.filterStatusLabel.Text = "Фильтр не активен";
            // 
            // spectrumPanel
            // 
            this.spectrumPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.spectrumPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spectrumPanel.Controls.Add(this.spectrumGraph);
            this.spectrumPanel.Controls.Add(this.spectrumNavigationScrollBar);
            this.spectrumPanel.Controls.Add(this.topTools);
            this.spectrumPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spectrumPanel.Location = new System.Drawing.Point(0, 0);
            this.spectrumPanel.Name = "spectrumPanel";
            this.spectrumPanel.Size = new System.Drawing.Size(1003, 237);
            this.spectrumPanel.TabIndex = 8;
            // 
            // spectrumGraph
            // 
            this.spectrumGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spectrumGraph.IsEnableHZoom = false;
            this.spectrumGraph.IsEnableVZoom = false;
            this.spectrumGraph.Location = new System.Drawing.Point(0, 25);
            this.spectrumGraph.Name = "spectrumGraph";
            this.spectrumGraph.ScrollGrace = 0D;
            this.spectrumGraph.ScrollMaxX = 0D;
            this.spectrumGraph.ScrollMaxY = 0D;
            this.spectrumGraph.ScrollMaxY2 = 0D;
            this.spectrumGraph.ScrollMinX = 0D;
            this.spectrumGraph.ScrollMinY = 0D;
            this.spectrumGraph.ScrollMinY2 = 0D;
            this.spectrumGraph.Size = new System.Drawing.Size(999, 191);
            this.spectrumGraph.TabIndex = 4;
            // 
            // spectrumNavigationScrollBar
            // 
            this.spectrumNavigationScrollBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.spectrumNavigationScrollBar.LargeChange = 125;
            this.spectrumNavigationScrollBar.Location = new System.Drawing.Point(0, 216);
            this.spectrumNavigationScrollBar.Maximum = 125;
            this.spectrumNavigationScrollBar.Name = "spectrumNavigationScrollBar";
            this.spectrumNavigationScrollBar.Size = new System.Drawing.Size(999, 17);
            this.spectrumNavigationScrollBar.SmallChange = 2;
            this.spectrumNavigationScrollBar.TabIndex = 3;
            this.spectrumNavigationScrollBar.Visible = false;
            this.spectrumNavigationScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.OnSpectrumNavigationBar_Scroll);
            // 
            // topTools
            // 
            this.topTools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spectrumHideBtn,
            this.spectrumZoomInBtn,
            this.spectrumZoomOutBtn,
            this.spectrumScaleSetter,
            this.spectrumChnSetter,
            this.spectrumSource,
            this.spectrumWinSetter});
            this.topTools.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.topTools.Location = new System.Drawing.Point(0, 0);
            this.topTools.Name = "topTools";
            this.topTools.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.topTools.Size = new System.Drawing.Size(999, 25);
            this.topTools.TabIndex = 1;
            this.topTools.Text = "toolStrip1";
            // 
            // spectrumHideBtn
            // 
            this.spectrumHideBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.spectrumHideBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.spectrumHideBtn.Image = global::Monitor.Properties.Resources.action_stop;
            this.spectrumHideBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.spectrumHideBtn.Name = "spectrumHideBtn";
            this.spectrumHideBtn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.spectrumHideBtn.Size = new System.Drawing.Size(23, 22);
            this.spectrumHideBtn.Tag = "spectrumHide";
            this.spectrumHideBtn.Text = "toolStripButton1";
            this.spectrumHideBtn.ToolTipText = "Закрыть";
            this.spectrumHideBtn.Click += new System.EventHandler(this.panelHideBtn_Click);
            // 
            // spectrumZoomInBtn
            // 
            this.spectrumZoomInBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.spectrumZoomInBtn.Image = global::Monitor.Properties.Resources.zoomIn;
            this.spectrumZoomInBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.spectrumZoomInBtn.Name = "spectrumZoomInBtn";
            this.spectrumZoomInBtn.Size = new System.Drawing.Size(23, 22);
            this.spectrumZoomInBtn.Tag = "ZoomInSpectrum";
            this.spectrumZoomInBtn.Text = "toolStripButton3";
            this.spectrumZoomInBtn.ToolTipText = "Увеличить";
            this.spectrumZoomInBtn.Click += new System.EventHandler(this.OnGraphZoomBtn_Click);
            // 
            // spectrumZoomOutBtn
            // 
            this.spectrumZoomOutBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.spectrumZoomOutBtn.Image = global::Monitor.Properties.Resources.zoomOut;
            this.spectrumZoomOutBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.spectrumZoomOutBtn.Name = "spectrumZoomOutBtn";
            this.spectrumZoomOutBtn.Size = new System.Drawing.Size(23, 22);
            this.spectrumZoomOutBtn.Tag = "ZoomOutSpectrum";
            this.spectrumZoomOutBtn.Text = "toolStripButton4";
            this.spectrumZoomOutBtn.ToolTipText = "Уменьшить";
            this.spectrumZoomOutBtn.Click += new System.EventHandler(this.OnGraphZoomBtn_Click);
            // 
            // spectrumScaleSetter
            // 
            this.spectrumScaleSetter.AutoCompleteCustomSource.AddRange(new string[] {
            "Линейная",
            "Логарифмическая"});
            this.spectrumScaleSetter.AutoToolTip = true;
            this.spectrumScaleSetter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.spectrumScaleSetter.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.spectrumScaleSetter.Items.AddRange(new object[] {
            "Линейная",
            "Логарифмическая в мВ",
            "Логарифмическая в дБ"});
            this.spectrumScaleSetter.Name = "spectrumScaleSetter";
            this.spectrumScaleSetter.Size = new System.Drawing.Size(160, 25);
            this.spectrumScaleSetter.ToolTipText = "Тип шкалы";
            this.spectrumScaleSetter.SelectedIndexChanged += new System.EventHandler(this.OnSpectrumAxisTypeChanged);
            // 
            // spectrumChnSetter
            // 
            this.spectrumChnSetter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.spectrumChnSetter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.spectrumChnSetter.Items.AddRange(new object[] {
            "Канал 1",
            "Канал 2"});
            this.spectrumChnSetter.Name = "spectrumChnSetter";
            this.spectrumChnSetter.Size = new System.Drawing.Size(80, 25);
            this.spectrumChnSetter.Tag = "SpectrumChannelChanged";
            this.spectrumChnSetter.ToolTipText = "Источник сигнала для спектра";
            this.spectrumChnSetter.SelectedIndexChanged += new System.EventHandler(this.OnSelectedItemChanged);
            // 
            // spectrumSource
            // 
            this.spectrumSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.spectrumSource.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.spectrumSource.Items.AddRange(new object[] {
            "Спектр до обработки",
            "Спектр после обработки"});
            this.spectrumSource.Name = "spectrumSource";
            this.spectrumSource.Size = new System.Drawing.Size(165, 25);
            this.spectrumSource.Tag = "SpectrumSource";
            this.spectrumSource.ToolTipText = "Отображаемый спектр";
            this.spectrumSource.SelectedIndexChanged += new System.EventHandler(this.OnSelectedItemChanged);
            // 
            // spectrumWinSetter
            // 
            this.spectrumWinSetter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.spectrumWinSetter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.spectrumWinSetter.Items.AddRange(new object[] {
            "Прямоугольное",
            "Синусное",
            "Ханна",
            "Хэмминга",
            "Блэкмана",
            "Наталла"});
            this.spectrumWinSetter.Name = "spectrumWinSetter";
            this.spectrumWinSetter.Size = new System.Drawing.Size(115, 25);
            this.spectrumWinSetter.Tag = "SpectrumWinSetter";
            this.spectrumWinSetter.ToolTipText = "Сглаживающее окно";
            this.spectrumWinSetter.SelectedIndexChanged += new System.EventHandler(this.OnSelectedItemChanged);
            // 
            // deviceSetsPanel
            // 
            this.deviceSetsPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deviceSetsPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.deviceSetsPanel.Controls.Add(this.toolStrip1);
            this.deviceSetsPanel.Controls.Add(this.label2);
            this.deviceSetsPanel.Controls.Add(this.deviceSpeedComboBox);
            this.deviceSetsPanel.Controls.Add(this.connectBtn);
            this.deviceSetsPanel.Controls.Add(this.label1);
            this.deviceSetsPanel.Controls.Add(this.deviceComboBox);
            this.deviceSetsPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.deviceSetsPanel.Location = new System.Drawing.Point(0, 0);
            this.deviceSetsPanel.MinimumSize = new System.Drawing.Size(247, 4);
            this.deviceSetsPanel.Name = "deviceSetsPanel";
            this.deviceSetsPanel.Size = new System.Drawing.Size(247, 167);
            this.deviceSetsPanel.TabIndex = 14;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deviceSetsHideBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(243, 25);
            this.toolStrip1.TabIndex = 13;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // deviceSetsHideBtn
            // 
            this.deviceSetsHideBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.deviceSetsHideBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deviceSetsHideBtn.Image = global::Monitor.Properties.Resources.action_stop;
            this.deviceSetsHideBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deviceSetsHideBtn.Name = "deviceSetsHideBtn";
            this.deviceSetsHideBtn.Size = new System.Drawing.Size(23, 22);
            this.deviceSetsHideBtn.Tag = "deviceSetsHide";
            this.deviceSetsHideBtn.Text = "Закрыть";
            this.deviceSetsHideBtn.Click += new System.EventHandler(this.panelHideBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Выберите скорость";
            // 
            // deviceSpeedComboBox
            // 
            this.deviceSpeedComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.deviceSpeedComboBox.Enabled = false;
            this.deviceSpeedComboBox.FormattingEnabled = true;
            this.deviceSpeedComboBox.Location = new System.Drawing.Point(121, 55);
            this.deviceSpeedComboBox.Name = "deviceSpeedComboBox";
            this.deviceSpeedComboBox.Size = new System.Drawing.Size(118, 21);
            this.deviceSpeedComboBox.TabIndex = 11;
            // 
            // connectBtn
            // 
            this.connectBtn.Enabled = false;
            this.connectBtn.Location = new System.Drawing.Point(140, 94);
            this.connectBtn.Name = "connectBtn";
            this.connectBtn.Size = new System.Drawing.Size(99, 23);
            this.connectBtn.TabIndex = 3;
            this.connectBtn.Text = "Подключить";
            this.connectBtn.UseVisualStyleBackColor = true;
            this.connectBtn.Click += new System.EventHandler(this.OnConnectDeviceBtnClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Выберите устройство";
            // 
            // deviceComboBox
            // 
            this.deviceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.deviceComboBox.Enabled = false;
            this.deviceComboBox.FormattingEnabled = true;
            this.deviceComboBox.Location = new System.Drawing.Point(121, 28);
            this.deviceComboBox.Name = "deviceComboBox";
            this.deviceComboBox.Size = new System.Drawing.Size(118, 21);
            this.deviceComboBox.TabIndex = 0;
            // 
            // spectrumTimer
            // 
            this.spectrumTimer.Interval = 3500;
            this.spectrumTimer.Tick += new System.EventHandler(this.OnSpectrumTimerTick);
            // 
            // mainContainer
            // 
            this.mainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainContainer.Location = new System.Drawing.Point(0, 24);
            this.mainContainer.Name = "mainContainer";
            this.mainContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainContainer.Panel1
            // 
            this.mainContainer.Panel1.Controls.Add(this.signalGraphsContainer);
            this.mainContainer.Panel1.Controls.Add(this.splitter1);
            this.mainContainer.Panel1.Controls.Add(this.deviceSetsPanel);
            this.mainContainer.Panel1MinSize = 106;
            // 
            // mainContainer.Panel2
            // 
            this.mainContainer.Panel2.Controls.Add(this.spectrumPanel);
            this.mainContainer.Panel2MinSize = 180;
            this.mainContainer.Size = new System.Drawing.Size(1003, 408);
            this.mainContainer.SplitterDistance = 167;
            this.mainContainer.TabIndex = 18;
            // 
            // signalGraphsContainer
            // 
            this.signalGraphsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.signalGraphsContainer.Location = new System.Drawing.Point(250, 0);
            this.signalGraphsContainer.Name = "signalGraphsContainer";
            // 
            // signalGraphsContainer.Panel1
            // 
            this.signalGraphsContainer.Panel1.Controls.Add(this.signalChn1Panel);
            this.signalGraphsContainer.Panel1MinSize = 250;
            // 
            // signalGraphsContainer.Panel2
            // 
            this.signalGraphsContainer.Panel2.Controls.Add(this.signalChn2Panel);
            this.signalGraphsContainer.Panel2MinSize = 250;
            this.signalGraphsContainer.Size = new System.Drawing.Size(753, 167);
            this.signalGraphsContainer.SplitterDistance = 359;
            this.signalGraphsContainer.SplitterWidth = 2;
            this.signalGraphsContainer.TabIndex = 19;
            // 
            // signalChn1Panel
            // 
            this.signalChn1Panel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.signalChn1Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.signalChn1Panel.Controls.Add(this.signalChn1Graph);
            this.signalChn1Panel.Controls.Add(this.chn1Tools);
            this.signalChn1Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.signalChn1Panel.Location = new System.Drawing.Point(0, 0);
            this.signalChn1Panel.Name = "signalChn1Panel";
            this.signalChn1Panel.Size = new System.Drawing.Size(359, 167);
            this.signalChn1Panel.TabIndex = 16;
            // 
            // signalChn1Graph
            // 
            this.signalChn1Graph.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.signalChn1Graph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.signalChn1Graph.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.signalChn1Graph.IsEnableHZoom = false;
            this.signalChn1Graph.IsEnableVZoom = false;
            this.signalChn1Graph.Location = new System.Drawing.Point(0, 25);
            this.signalChn1Graph.Name = "signalChn1Graph";
            this.signalChn1Graph.ScrollGrace = 0D;
            this.signalChn1Graph.ScrollMaxX = 0D;
            this.signalChn1Graph.ScrollMaxY = 0D;
            this.signalChn1Graph.ScrollMaxY2 = 0D;
            this.signalChn1Graph.ScrollMinX = 0D;
            this.signalChn1Graph.ScrollMinY = 0D;
            this.signalChn1Graph.ScrollMinY2 = 0D;
            this.signalChn1Graph.Size = new System.Drawing.Size(355, 138);
            this.signalChn1Graph.TabIndex = 1;
            // 
            // chn1Tools
            // 
            this.chn1Tools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.signalChn1HideBtn,
            this.signalChn1ZoomInBtn,
            this.signalChn1ZoomOutBtn,
            this.signalChn1Source,
            this.middleFilterChn1ActivityLed,
            this.stopbandFilterChn1ActivityLed});
            this.chn1Tools.Location = new System.Drawing.Point(0, 0);
            this.chn1Tools.Name = "chn1Tools";
            this.chn1Tools.Size = new System.Drawing.Size(355, 25);
            this.chn1Tools.TabIndex = 0;
            this.chn1Tools.Tag = "chn1Tools";
            this.chn1Tools.Text = "toolStrip2";
            // 
            // signalChn1HideBtn
            // 
            this.signalChn1HideBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.signalChn1HideBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.signalChn1HideBtn.Image = global::Monitor.Properties.Resources.action_stop;
            this.signalChn1HideBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.signalChn1HideBtn.Name = "signalChn1HideBtn";
            this.signalChn1HideBtn.Size = new System.Drawing.Size(23, 22);
            this.signalChn1HideBtn.Tag = "signalChn1Hide";
            this.signalChn1HideBtn.Text = "toolStripButton1";
            this.signalChn1HideBtn.ToolTipText = "Закрыть";
            this.signalChn1HideBtn.Click += new System.EventHandler(this.panelHideBtn_Click);
            // 
            // signalChn1ZoomInBtn
            // 
            this.signalChn1ZoomInBtn.CheckOnClick = true;
            this.signalChn1ZoomInBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.signalChn1ZoomInBtn.Image = global::Monitor.Properties.Resources.zoomIn;
            this.signalChn1ZoomInBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.signalChn1ZoomInBtn.Name = "signalChn1ZoomInBtn";
            this.signalChn1ZoomInBtn.Size = new System.Drawing.Size(23, 22);
            this.signalChn1ZoomInBtn.Tag = "ZoomInChn1";
            this.signalChn1ZoomInBtn.Text = "toolStripButton1";
            this.signalChn1ZoomInBtn.ToolTipText = "Увеличить";
            this.signalChn1ZoomInBtn.Click += new System.EventHandler(this.OnGraphZoomBtn_Click);
            // 
            // signalChn1ZoomOutBtn
            // 
            this.signalChn1ZoomOutBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.signalChn1ZoomOutBtn.Image = global::Monitor.Properties.Resources.zoomOut;
            this.signalChn1ZoomOutBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.signalChn1ZoomOutBtn.Name = "signalChn1ZoomOutBtn";
            this.signalChn1ZoomOutBtn.Size = new System.Drawing.Size(23, 22);
            this.signalChn1ZoomOutBtn.Tag = "ZoomOutChn1";
            this.signalChn1ZoomOutBtn.Text = "toolStripButton2";
            this.signalChn1ZoomOutBtn.ToolTipText = "Уменьшить";
            this.signalChn1ZoomOutBtn.Click += new System.EventHandler(this.OnGraphZoomBtn_Click);
            // 
            // signalChn1Source
            // 
            this.signalChn1Source.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.signalChn1Source.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.signalChn1Source.Items.AddRange(new object[] {
            "Сигнал до обработки",
            "Сигнал после обработки"});
            this.signalChn1Source.Name = "signalChn1Source";
            this.signalChn1Source.Size = new System.Drawing.Size(165, 25);
            this.signalChn1Source.Tag = "SignalChn1Source";
            this.signalChn1Source.ToolTipText = "Отображаемый сигнал";
            this.signalChn1Source.SelectedIndexChanged += new System.EventHandler(this.OnSelectedItemChanged);
            // 
            // middleFilterChn1ActivityLed
            // 
            this.middleFilterChn1ActivityLed.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.middleFilterChn1ActivityLed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.middleFilterChn1ActivityLed.Image = global::Monitor.Properties.Resources.redLed;
            this.middleFilterChn1ActivityLed.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.middleFilterChn1ActivityLed.Name = "middleFilterChn1ActivityLed";
            this.middleFilterChn1ActivityLed.Size = new System.Drawing.Size(16, 22);
            this.middleFilterChn1ActivityLed.Text = "toolStripLabel1";
            this.middleFilterChn1ActivityLed.ToolTipText = "Активность ПФ";
            // 
            // stopbandFilterChn1ActivityLed
            // 
            this.stopbandFilterChn1ActivityLed.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.stopbandFilterChn1ActivityLed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stopbandFilterChn1ActivityLed.Image = global::Monitor.Properties.Resources.redLed;
            this.stopbandFilterChn1ActivityLed.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.stopbandFilterChn1ActivityLed.Name = "stopbandFilterChn1ActivityLed";
            this.stopbandFilterChn1ActivityLed.Size = new System.Drawing.Size(16, 22);
            this.stopbandFilterChn1ActivityLed.Text = "toolStripLabel2";
            this.stopbandFilterChn1ActivityLed.ToolTipText = "Активность РФ";
            // 
            // signalChn2Panel
            // 
            this.signalChn2Panel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.signalChn2Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.signalChn2Panel.Controls.Add(this.signalChn2Graph);
            this.signalChn2Panel.Controls.Add(this.chn2Tools);
            this.signalChn2Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.signalChn2Panel.Location = new System.Drawing.Point(0, 0);
            this.signalChn2Panel.Name = "signalChn2Panel";
            this.signalChn2Panel.Size = new System.Drawing.Size(392, 167);
            this.signalChn2Panel.TabIndex = 17;
            // 
            // signalChn2Graph
            // 
            this.signalChn2Graph.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.signalChn2Graph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.signalChn2Graph.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.signalChn2Graph.IsEnableHZoom = false;
            this.signalChn2Graph.IsEnableVZoom = false;
            this.signalChn2Graph.Location = new System.Drawing.Point(0, 25);
            this.signalChn2Graph.Name = "signalChn2Graph";
            this.signalChn2Graph.ScrollGrace = 0D;
            this.signalChn2Graph.ScrollMaxX = 0D;
            this.signalChn2Graph.ScrollMaxY = 0D;
            this.signalChn2Graph.ScrollMaxY2 = 0D;
            this.signalChn2Graph.ScrollMinX = 0D;
            this.signalChn2Graph.ScrollMinY = 0D;
            this.signalChn2Graph.ScrollMinY2 = 0D;
            this.signalChn2Graph.Size = new System.Drawing.Size(388, 138);
            this.signalChn2Graph.TabIndex = 1;
            // 
            // chn2Tools
            // 
            this.chn2Tools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.signalChn2HideBtn,
            this.signalChn2ZoomInBtn,
            this.signalChn2ZoomOutBtn,
            this.signalChn2Source,
            this.middleFilterChn2ActivityLed,
            this.stopbandFilterChn2ActivityLed});
            this.chn2Tools.Location = new System.Drawing.Point(0, 0);
            this.chn2Tools.Name = "chn2Tools";
            this.chn2Tools.Size = new System.Drawing.Size(388, 25);
            this.chn2Tools.TabIndex = 0;
            this.chn2Tools.Tag = "chn2Tools";
            this.chn2Tools.Text = "toolStrip2";
            // 
            // signalChn2HideBtn
            // 
            this.signalChn2HideBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.signalChn2HideBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.signalChn2HideBtn.Image = global::Monitor.Properties.Resources.action_stop;
            this.signalChn2HideBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.signalChn2HideBtn.Name = "signalChn2HideBtn";
            this.signalChn2HideBtn.Size = new System.Drawing.Size(23, 22);
            this.signalChn2HideBtn.Tag = "signalChn2Hide";
            this.signalChn2HideBtn.Text = "toolStripButton1";
            this.signalChn2HideBtn.ToolTipText = "Закрыть";
            this.signalChn2HideBtn.Click += new System.EventHandler(this.panelHideBtn_Click);
            // 
            // signalChn2ZoomInBtn
            // 
            this.signalChn2ZoomInBtn.CheckOnClick = true;
            this.signalChn2ZoomInBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.signalChn2ZoomInBtn.Image = global::Monitor.Properties.Resources.zoomIn;
            this.signalChn2ZoomInBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.signalChn2ZoomInBtn.Name = "signalChn2ZoomInBtn";
            this.signalChn2ZoomInBtn.Size = new System.Drawing.Size(23, 22);
            this.signalChn2ZoomInBtn.Tag = "ZoomInChn2";
            this.signalChn2ZoomInBtn.Text = "toolStripButton1";
            this.signalChn2ZoomInBtn.ToolTipText = "Увеличить";
            this.signalChn2ZoomInBtn.Click += new System.EventHandler(this.OnGraphZoomBtn_Click);
            // 
            // signalChn2ZoomOutBtn
            // 
            this.signalChn2ZoomOutBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.signalChn2ZoomOutBtn.Image = global::Monitor.Properties.Resources.zoomOut;
            this.signalChn2ZoomOutBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.signalChn2ZoomOutBtn.Name = "signalChn2ZoomOutBtn";
            this.signalChn2ZoomOutBtn.Size = new System.Drawing.Size(23, 22);
            this.signalChn2ZoomOutBtn.Tag = "ZoomOutChn2";
            this.signalChn2ZoomOutBtn.Text = "toolStripButton2";
            this.signalChn2ZoomOutBtn.ToolTipText = "Уменьшить";
            this.signalChn2ZoomOutBtn.Click += new System.EventHandler(this.OnGraphZoomBtn_Click);
            // 
            // signalChn2Source
            // 
            this.signalChn2Source.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.signalChn2Source.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.signalChn2Source.Items.AddRange(new object[] {
            "Сигнал до обработки ",
            "Сигнал после обработки"});
            this.signalChn2Source.Name = "signalChn2Source";
            this.signalChn2Source.Size = new System.Drawing.Size(165, 25);
            this.signalChn2Source.Tag = "SignalChn2Source";
            this.signalChn2Source.ToolTipText = "Отображаемый сигнал";
            this.signalChn2Source.SelectedIndexChanged += new System.EventHandler(this.OnSelectedItemChanged);
            // 
            // middleFilterChn2ActivityLed
            // 
            this.middleFilterChn2ActivityLed.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.middleFilterChn2ActivityLed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.middleFilterChn2ActivityLed.Image = global::Monitor.Properties.Resources.redLed;
            this.middleFilterChn2ActivityLed.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.middleFilterChn2ActivityLed.Name = "middleFilterChn2ActivityLed";
            this.middleFilterChn2ActivityLed.Size = new System.Drawing.Size(16, 22);
            this.middleFilterChn2ActivityLed.Text = "toolStripLabel1";
            this.middleFilterChn2ActivityLed.ToolTipText = "Активность ПФ";
            // 
            // stopbandFilterChn2ActivityLed
            // 
            this.stopbandFilterChn2ActivityLed.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.stopbandFilterChn2ActivityLed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stopbandFilterChn2ActivityLed.Image = global::Monitor.Properties.Resources.redLed;
            this.stopbandFilterChn2ActivityLed.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.stopbandFilterChn2ActivityLed.Name = "stopbandFilterChn2ActivityLed";
            this.stopbandFilterChn2ActivityLed.Size = new System.Drawing.Size(16, 22);
            this.stopbandFilterChn2ActivityLed.Text = "toolStripLabel2";
            this.stopbandFilterChn2ActivityLed.ToolTipText = "Активность РФ";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(247, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 167);
            this.splitter1.TabIndex = 18;
            this.splitter1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1003, 454);
            this.Controls.Add(this.mainContainer);
            this.Controls.Add(this.statusLable);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(644, 380);
            this.Name = "MainForm";
            this.Text = "Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusLable.ResumeLayout(false);
            this.statusLable.PerformLayout();
            this.spectrumPanel.ResumeLayout(false);
            this.spectrumPanel.PerformLayout();
            this.topTools.ResumeLayout(false);
            this.topTools.PerformLayout();
            this.deviceSetsPanel.ResumeLayout(false);
            this.deviceSetsPanel.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.mainContainer.Panel1.ResumeLayout(false);
            this.mainContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainContainer)).EndInit();
            this.mainContainer.ResumeLayout(false);
            this.signalGraphsContainer.Panel1.ResumeLayout(false);
            this.signalGraphsContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.signalGraphsContainer)).EndInit();
            this.signalGraphsContainer.ResumeLayout(false);
            this.signalChn1Panel.ResumeLayout(false);
            this.signalChn1Panel.PerformLayout();
            this.chn1Tools.ResumeLayout(false);
            this.chn1Tools.PerformLayout();
            this.signalChn2Panel.ResumeLayout(false);
            this.signalChn2Panel.PerformLayout();
            this.chn2Tools.ResumeLayout(false);
            this.chn2Tools.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem portSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spectrumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signalToolStripMenuItem;
        private Timer signalTimer;
        private StatusStrip statusLable;
        private Panel spectrumPanel;
        private ToolStrip topTools;
        private ToolStripButton spectrumHideBtn;
        private ToolStripButton spectrumZoomInBtn;
        private ToolStripButton spectrumZoomOutBtn;
        private ToolStripStatusLabel statusLabel;
        private Panel deviceSetsPanel;
        private System.Windows.Forms.Label label2;
        private ComboBox deviceSpeedComboBox;
        private Button connectBtn;
        private System.Windows.Forms.Label label1;
        private ComboBox deviceComboBox;
        private Timer spectrumTimer;
        private HScrollBar spectrumNavigationScrollBar;
        private ZedGraphControl spectrumGraph;
        private ToolStripComboBox spectrumScaleSetter;
        private ToolStripMenuItem filterToolStripMenuItem;
        private ToolStripMenuItem middleFilterActiveStateMenuItm;
        private ToolStripMenuItem filterSettingMenuItem;
        private ToolStripMenuItem chn1ToolStripMenuItem;
        private ToolStripMenuItem chn2ToolStripMenuItem;
        private ToolStripStatusLabel filterStatusLabel;
        private ToolStripComboBox spectrumChnSetter;
        private ToolStripComboBox spectrumSource;
        private ToolStripComboBox spectrumWinSetter;
        private ToolStripMenuItem stopbandActiveStateMenuItm;
        private ToolStripMenuItem passbandChn1ActiveStateMnuItm;
        private ToolStripMenuItem passbandChn2ActiveStateMnuItm;
        private ToolStripMenuItem stopbandChn1ActiveStateMnuItm;
        private ToolStripMenuItem stopbandChn2ActiveStateMnuItm;
        private SplitContainer mainContainer;
        private SplitContainer signalGraphsContainer;
        private Panel signalChn1Panel;
        private ZedGraphControl signalChn1Graph;
        private ToolStrip chn1Tools;
        private ToolStripButton signalChn1HideBtn;
        private ToolStripButton signalChn1ZoomInBtn;
        private ToolStripButton signalChn1ZoomOutBtn;
        private ToolStripComboBox signalChn1Source;
        private Panel signalChn2Panel;
        private ZedGraphControl signalChn2Graph;
        private ToolStrip chn2Tools;
        private ToolStripButton signalChn2HideBtn;
        private ToolStripButton signalChn2ZoomInBtn;
        private ToolStripButton signalChn2ZoomOutBtn;
        private ToolStripComboBox signalChn2Source;
        private Splitter splitter1;
        private ToolStripMenuItem middleFilterTypeMenuItem;
        private ToolStripMenuItem middleFilterPassbandMnuItm;
        private ToolStripMenuItem middleFilterLowpassMnuItm;
        private ToolStripLabel middleFilterChn1ActivityLed;
        private ToolStripLabel stopbandFilterChn1ActivityLed;
        private ToolStripLabel middleFilterChn2ActivityLed;
        private ToolStripLabel stopbandFilterChn2ActivityLed;
        private ToolStrip toolStrip1;
        private ToolStripButton deviceSetsHideBtn;
    }
}

