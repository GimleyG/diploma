﻿using Monitor.CommonTypes;
using Monitor.Model.SignalProcessing;

namespace Monitor.AppSettings
{
    public static class AvailibleFilters
    {
        private static FilterSettings[,] _filters;
        private static FilterSettings _lastModified;

        static AvailibleFilters()
        {
            _filters = new FilterSettings[3, 2];
            DefaultSetup();
            _lastModified =_filters[0, 0];
        }

        private static void DefaultSetup()
        {
            _filters[0, 0] = new FilterSettings()
                        {
                            TypeOfFilter = FilterType.Passband,
                            CurrentChannel = Channel.Chn1,
                            FilterOrder = 400,
                            FrequencyRange = new Range { Min = 7.5, Max = 12.5 },
                            WinRate = 0.04
                        };
            _filters[0, 1] = new FilterSettings()
                        {
                            TypeOfFilter = FilterType.Passband,
                            CurrentChannel = Channel.Chn2,
                            FilterOrder = 400,
                            FrequencyRange = new Range { Min = 7.5, Max = 12.5 },
                            WinRate = 0.04
                        };

            _filters[1, 0] = new FilterSettings()
                        {
                            TypeOfFilter = FilterType.Stopband,
                            CurrentChannel = Channel.Chn1,
                            FilterOrder = 2048,
                            FrequencyRange = new Range { Min = 49, Max = 51 },
                            WinRate = 0.001
                        };
            _filters[1, 1] = new FilterSettings()
                        {
                            TypeOfFilter = FilterType.Stopband,
                            CurrentChannel = Channel.Chn2,
                            FilterOrder = 2048,
                            FrequencyRange = new Range { Min = 49, Max = 51 },
                            WinRate = 0.001
                        };

            _filters[2, 0] = new FilterSettings()
                        {
                            TypeOfFilter = FilterType.Lowpass,
                            CurrentChannel = Channel.Chn1,
                            FilterOrder = 500,
                            FrequencyRange = new Range { Min = 0, Max = 15 },
                            WinRate = 0.04
                        };
            _filters[2, 1] = new FilterSettings()
                        {
                            TypeOfFilter = FilterType.Lowpass,
                            CurrentChannel = Channel.Chn2,
                            FilterOrder = 500,
                            FrequencyRange = new Range { Min = 0, Max = 15 },
                            WinRate = 0.04
                        };                
        }

        public static void SaveChanges(int order, Range freqRange, double winRate, FilterType type, Channel channel)
        {
            _filters[(int)type, (int)channel].SetNewParameters(order, freqRange, winRate, type, channel);
            _lastModified = _filters[(int)type, (int)channel];
        }

        public static IFilterParamsPresenter LoadChangesFor(FilterType type, Channel channel)
        {
            return _filters[(int)type, (int)channel];
        }

        public static IFilterParamsPresenter LastModified
        {
            get 
            {
                return _lastModified;
            }
            private set {}
        }
    }
}
